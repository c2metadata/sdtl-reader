Tools
------

Several tools can be used to extract transforms from R code.

* :doc:`SDTL Reader desktop application </sdtl-reader/index>`
* :doc:`SDTL Reader command line application </spss-to-sdtl-console/index>`
* SDTL Web Application (documentation forthcoming)
