.. SDTL Tools documentation master file, created by
   sphinx-quickstart on Fri Nov 10 08:26:00 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SDTL Tools Documentation
########################################

.. toctree::
   :maxdepth: 2
   :caption: Contents

   sdtl-reader/index
   sdtl-converter-console/index
   spss-syntax-support/index
   r-syntax-support/index
