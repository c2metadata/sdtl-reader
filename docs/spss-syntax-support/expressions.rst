Expressions
-----------

=================  ==========================  ======
Command            Description                 Status
=================  ==========================  ======
Numeric literal    1                           Implemented
String literal     "Hello"                     Implemented
Symbol             var1                        Implemented
Addition           1 + var1                    Implemented
Subtraction        var1 - 10                   Implemented
Multiplication     var1 * 10                   Implemented
Division           var1 / var2                 Implemented
Exponent           var1 ** 2                   Implemented
Function call      MEAN(var1, var2)            Implemented
Parenthesized      (1 + MEAN(var1, var2)) * 3  Implemented
=================  ==========================  ======

