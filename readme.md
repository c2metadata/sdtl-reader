# SDTL Tools for SPSS and R

These tools extract and understand transforms from SPSS command (.sps) and R
(.r, .rmd) files, and represent them in Structured Data Transform Language
(SDTL).

## Build Status and Downloads

| Platform   | Status                                                                                                                                              |
| ---------- | --------                                                                                                                                            |
| Windows    | [![Build status](https://ci.appveyor.com/api/projects/status/ipyb382r3i5dusx4?svg=true)](https://ci.appveyor.com/project/JeremyIverson/sdtl-reader) |
| macOS      | Forthcoming                                                                                                                                         |
| Linux      | Forthcoming                                                                                                                                         |

## Desktop Application

The desktop tool can be downloaded using the link above, and navigating the Artifacts area.

## Command Line Application

The command line tool can be downloaded from the link above, and navigating to the artifacts area.
A dotnet tool installer will be available in the future, for simpler installation on multiple platforms.

## Web Application

### Use the Docker Image

A docker image containing a basic web application can be downloaded from 
https://gitlab.com/c2metadata/sdtl-reader/container_registry

    docker pull registry.gitlab.com/c2metadata/sdtl-reader:latest

    docker run -d -p 8080:80 registry.gitlab.com/c2metadata/sdtl-reader
    
### API Endpoints

SPSS: `/api/spsstosdtl`

    curl -X POST "http://localhost:8080/api/spsstosdtl" -d '{ "parameters": { "spss": "COMPUTE x = 1." } }'

    

R: `/api/rtosdtl`

    curl -X POST "http://localhost:8080/api/rtosdtl" -d '{ "parameters": { "r": "rename(flights, tail_num = tailnum)" } }'

## Example JSON Instances

Some example JSON instances are created by the tools every time an update is
made to those tools.

* [CPS transforms example](http://ci.appveyor.com/api/projects/JeremyIverson/sdtl-reader/artifacts/src/C2Metadata.Cli/cps-demo.sdtl.json)
* [Expression example](http://ci.appveyor.com/api/projects/JeremyIverson/sdtl-reader/artifacts/src/C2Metadata.Cli/expression-demo.sdtl.json)

## SPSS Support

Current support includes:

* Get
* Save
* Title
* Subtitle
* Recode
* Rename Variables
* Execute

Up next:

* Unconditional Assignments
* Conditional assignments
* Delete Variables
* Formats
* Compute
* Add Value Labels
* Variable Labels
* Conditional Recodes

Needs improvement:

* Recode handling (for example, conditional recodes)
* Save: drop, keep, and rename variables

## R Support

Information forthcoming
