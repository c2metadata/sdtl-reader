﻿* Encoding: UTF-8.
* This SPSS file demonstrates some transformations.
* It uses the Current Population Survey data.

GET FILE="U:\cps.sav".

TITLE "Current Population Survey Demonstration".
SUBTITLE "Demonstrate various data transforms".

DELETE VARIABLES PXPDEMP2.
DELETE VARIABLES PXPDEMP1 TO FILLER.

* Subset fully completed interviews for Minnesotans.
SELECT IF HUFINAL = 001.
SELECT IF GESTFIPS = 27.

* Set some value labels.
* TODO this variable does not actually exist in the dataset. Update with a proper example.
VALUE LABELS HHinc_cat 
  1.00 'Less than $25,000' 
  2.00 '$25,000-$50,000' 
  3.00 '$50,000-$75,000' 
  4.00 '$75,000-$100,000' 
  5.00 '$100,000-$150,000' 
  6.00 'Greater than $150,000'. 

* Compute a new variable.
COMPUTE new1 = GESTFIPS + 10.

* Perform some recodes.
* TODO this variable does not actually exist in the dataset. Update with a proper example.
RECODE V520131 (0=0) (1,2=1) (3 thru 6=2) (7,8=3) into EDUC2.

* Indicate what to do with the values that are not being recoded (keep or drop). 
RECODE v2 (1=2) (else=copy) into rec_v2.

* Recode with a data type change (in this example string to numeric)
RECODE s1 ('Very bad' = 1)('Bad' = 2) ('Neutral' = 3)('Good' = 4)('Very good' = 5) into n1.

* Save.
SAVE OUTFILE="U:\cps-transformed.sav".



