﻿using C2Metadata.Common.RConverter;
using C2Metadata.SpssToSdtl.Tests.Utility;
using sdtl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace C2Metadata.SpssToSdtl.Tests.R
{
    public class UseCaseTests
    {
        private ITestOutputHelper output;

        public UseCaseTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void RTest1()
        {
            string fileName = @"c:/svn/ccmetadata/r-examples/dplyr-example.r";
            this.output.WriteLine("Testing " + fileName);

            var converter = new RConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            int lines = program.Commands.Count;
            this.output.WriteLine($"{lines} lines");

            TestHelper.SaveJson(program, @"c:\out\r1.sdtl.json");

        }

        [Fact]
        public void RTest2()
        {
            string fileName = @"c:/svn/ccmetadata/r-examples/recode/Recode_2ways.R";
            this.output.WriteLine("Testing " + fileName);

            var converter = new RConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            int lines = program.Commands.Count;
            this.output.WriteLine($"{lines} lines");

            TestHelper.SaveJson(program, @"c:\out\r2.sdtl.json");
        }

        [Fact]
        public void RTest3()
        {
            string fileName = @"c:/svn/ccmetadata/r-examples/knb/subsistence_harvest_processing.Rmd";
            this.output.WriteLine("Testing " + fileName);

            var converter = new RConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            int lines = program.Commands.Count;
            this.output.WriteLine($"{lines} lines");

            TestHelper.SaveJson(program, @"d:\out\r2.sdtl.json");
        }

        [Fact]
        public void RTest4()
        {
            string fileName = @"d:/svn/ccmetadata/r-examples/small_test/small_test.r";
            this.output.WriteLine("Testing " + fileName);

            var converter = new RConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            Assert.Equal(13, program.Commands.Count);

            Assert.IsType<NoTransformOp>(program.Commands[0]);
            Assert.IsType<NoTransformOp>(program.Commands[1]);
            Assert.IsType<NoTransformOp>(program.Commands[2]);
            Assert.IsType<NoTransformOp>(program.Commands[3]);
            Assert.IsType<Load>(program.Commands[4]);
            Assert.IsType<Analysis>(program.Commands[5]);
            Assert.IsType<Compute>(program.Commands[6]);
            Assert.IsType<Compute>(program.Commands[7]);
            Assert.IsType<Compute>(program.Commands[8]);
            Assert.IsType<Rename>(program.Commands[9]);
            Assert.IsType<SetVariableLabel>(program.Commands[10]);
            Assert.IsType<SetValueLabels>(program.Commands[11]);
            Assert.IsType<SetVariableLabel>(program.Commands[12]);

            TestHelper.SaveJson(program, @"d:\out\small_test.sdtl.json");
        }

        [Fact]
        public void RTest5()
        {
            string fileName = @"d:/svn/ccmetadata/r-examples/knb/All_Harvest.Rmd";
            this.output.WriteLine("Testing " + fileName);

            var converter = new RConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            //Assert.Equal(13, program.Commands.Count);

            Assert.IsType<NoTransformOp>(program.Commands[0]);
            Assert.IsType<NoTransformOp>(program.Commands[1]);
            Assert.IsType<NoTransformOp>(program.Commands[2]);
            Assert.IsType<NoTransformOp>(program.Commands[3]);
            Assert.IsType<NoTransformOp>(program.Commands[4]);
            Assert.IsType<NoTransformOp>(program.Commands[5]);
            Assert.IsType<Load>(program.Commands[6]);


            //Assert.IsType<Analysis>(program.Commands[5]);
            //Assert.IsType<Compute>(program.Commands[6]);
            //Assert.IsType<Compute>(program.Commands[7]);
            //Assert.IsType<Compute>(program.Commands[8]);
            //Assert.IsType<Rename>(program.Commands[9]);
            //Assert.IsType<SetVariableLabel>(program.Commands[10]);
            //Assert.IsType<SetValueLabels>(program.Commands[11]);
            //Assert.IsType<SetVariableLabel>(program.Commands[12]);

            TestHelper.SaveJson(program, @"d:\out\All_Harvest.sdtl.json");
        }

        [Fact]
        public void TestSnippet1()
        {
            string content = @"s <- sport %>% 
filter(TYPE == ""0 - EST"") %>% 
rename(SASAP.Region = region_name) %>%
mutate(SASAP.Region = ifelse(SASAP.Region == ""Aleutian Islands"", ""Alaska Peninsula and Aleutian Islands"", SASAP.Region)) %>%
filter(num_responses > 12 | is.na(num_responses) == T) %>%
group_by(year, SASAP.Region, species) %>%
summarize(harvest = sum(harvest, na.rm = T)) %>%
mutate(Sector = ""Sport Fish"")";

            var converter = new RConverter();
            var program = converter.ConvertString(content, true);
            Assert.NotNull(program);

            TestHelper.SaveJson(program, @"d:\out\snippet1.sdtl.json");
        }
                
    

        [Fact]
        public void DataOne_FullTest()
        {
            Dictionary<string, int> counts = new Dictionary<string, int>();
            foreach (string fileName in Directory.GetFiles("c:/svn/ccmetadata/r-examples/knb"))
            {
                this.output.WriteLine("Testing " + fileName);

                var converter = new RConverter();
                var program = converter.ConvertFile(fileName, true);
                Assert.NotNull(program);

                int lines = program.Commands.Count;
                this.output.WriteLine($"{lines} lines");

                foreach (var pair in converter.FunctionCounts)
                {
                    if (counts.ContainsKey(pair.Key))
                    {
                        counts[pair.Key] += pair.Value;
                    }
                    else
                    {
                        counts.Add(pair.Key, pair.Value);
                    }
                }
            }

            string functionCounts = string.Join("\n", 
                counts
                    .OrderByDescending(x => x.Value)
                    .Select(x => $"{x.Key}: {x.Value}"));
            this.output.WriteLine(functionCounts);
        }
    }
}
