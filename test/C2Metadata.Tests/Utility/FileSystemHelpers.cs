﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace C2Metadata.SpssToSdtl.Tests.Utility
{
    public static class FileSystemHelpers
    {
        public static List<string> GatherSpssCommandFiles(string path)
        {
            var paths = new List<string>();

            foreach (string file in Directory.GetFiles(path))
            {
                if (file.ToLower().EndsWith(".sps"))
                {
                    paths.Add(file);
                }
            }

            foreach (string dir in Directory.GetDirectories(path))
            {
                var morePaths = GatherSpssCommandFiles(dir);
                paths.AddRange(morePaths);
            }

            return paths;
        }

    }
}
