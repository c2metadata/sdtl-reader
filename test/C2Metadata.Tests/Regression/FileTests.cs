﻿using C2Metadata.Common.SpssConverter;
using C2Metadata.SpssToSdtl.Tests.Utility;
using sdtl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace C2Metadata.SpssToSdtl.Tests.Regression
{
    public class FileTests
    {
        private ITestOutputHelper output;

        public FileTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Issue_29()
        {
            string syntax = @"*create new variable using compute with log function.  
*declare print formats on the new variable.  
*assign label to (new) variable.  

get file='da07213_inputForLog.sav'.  
compute Logpop = lg10(V520006).  
print formats Logpop (f3.2).  
variable labels Logpop 'Log Population of PSU'.  
save outfile='da07213_computeLog.sav'.    
execute.
";

            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);

            int lines = program.Commands.Count;


        }

        [Fact]
        public void Issue_34()
        {
            string syntax = @"*rename variables in a dataset

get file=""da7568 - 1.sav"".
rename variables
 / (V4 = CASEID)
 / (V8 = SEX)
 / (V9 = RACE)
 / (V10 = AGE)
 / (V11 = MARITAL)
 / (V12 = HEALTHDEFECT)
 / (V14 = READING)
 / (V15 = WRITING).
save outfile = ""da7568-1_rename.sav"".
";

            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);


            Assert.Equal(4, program.Commands.Count);
            Assert.IsType<Comment>(program.Commands[0]);
            Assert.IsType<Load>(program.Commands[1]);
            Assert.IsType<Rename>(program.Commands[2]);
            Assert.IsType<Save>(program.Commands[3]);


        }

        [Fact]
        public void George_20181005_CommentOnFirstLine()
        {
            string syntax = @"***** Comment on first line.
COMPUTE x = 10.";

            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);

            TestHelper.SaveJson(program, @"c:\out\comment-on-first-line.sdtl.json.txt");

            Assert.Equal(2, program.Commands.Count);
            Assert.IsType<Comment>(program.Commands[0]);
            Assert.IsType<Compute>(program.Commands[1]);
        }

        [Fact]
        public void Decimal_Numbers_No_Leading_0_1()
        {
            string syntax = @"COMPUTE agestd = .07.";

            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);

            Assert.Empty(program.Messages);
            Assert.Single(program.Commands);
            Assert.IsType<Compute>(program.Commands[0]);

            var compute = program.Commands[0] as Compute;
            var number = compute.Expression as NumericConstantExpression;

            Assert.Equal("double", number.NumericType);
            Assert.Equal(".07", number.Value);
        }

        [Fact]
        public void Decimal_Numbers_No_Leading_0_2()
        {
            string syntax = @"COMPUTE agestd = 45.99 / .07.";

            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);

            Assert.Empty(program.Messages);
            Assert.Single(program.Commands);
            Assert.IsType<Compute>(program.Commands[0]);

            var compute = program.Commands[0] as Compute;
            var division = compute.Expression as FunctionCallExpression;
            var number = division.Arguments[1].ArgumentValue as NumericConstantExpression;

            Assert.Equal("double", number.NumericType);
            Assert.Equal(".07", number.Value);
        }

        [Fact]
        public void Decimal_Numbers_No_Leading_0_3()
        {
            string syntax = @"COMPUTE agestd = (age - 45.99) / .07.";

            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);

            Assert.Empty(program.Messages);
            Assert.Single(program.Commands);
            Assert.IsType<Compute>(program.Commands[0]);

            var compute = program.Commands[0] as Compute;
            var division = compute.Expression as FunctionCallExpression;
            var number = division.Arguments[1].ArgumentValue as NumericConstantExpression;

            Assert.Equal("double", number.NumericType);
            Assert.Equal(".07", number.Value);
        }

        [Fact]
        public void George_20181029_1()
        {
             string syntax = @"compute age_comp = 10*trunc(age/10).
VARIABLE LABELS age_comp ""Age categorized by COMPUTE"".
COMPUTE mn_party=MEAN(PARTYID1,PARTYID2,PARTYID3).
COMPUTE agestd = ((age - 45.99)/.07).";

            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);

            Assert.Empty(program.Messages);
            Assert.Equal(4, program.Commands.Count);
            Assert.IsType<Compute>(program.Commands[0]);

        }

        [Fact]
        public void Issue_63()
        {
            string syntax = @"*create a new variable that counts ""yes"" answers to four source variables to 
*create a total count of the sources used by respondents to get information about the political campaign.
*assign variable label to this new variable.
get file='da07213_useForDo_Repeat.sav'.
compute Info=0.
do repeat a=V520173 to V520176.
if ((a ge 1) and (a le 3)) Info=Info+1.
end repeat.
var label Info 'Number of Information Sources'.
save outfile='da07213_DoRepeat_loop.sav'.
execute.";

            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);

            Assert.IsType<LoopOverList>(program.Commands[4]);
            var loop = program.Commands[4] as LoopOverList;

            Assert.IsType<IfRows>(loop.Commands[0]);
            var ifCommand = loop.Commands[0] as IfRows;
            Assert.Single(ifCommand.ThenCommands);
            Assert.IsType<Compute>(ifCommand.ThenCommands[0]);
            var compute = ifCommand.ThenCommands[0] as Compute;

            Assert.IsType<FunctionCallExpression>(compute.Expression);
            var functionCall = compute.Expression as FunctionCallExpression;

            Assert.Equal("FunctionCallExpression", functionCall.TypeDescriminator);
        }

        [Fact]
        public void Issue_43()
        {
            string syntax = @"*example of ""match files*command.
get file = 'da07213_aggregate2.sav'.
get file = 'da07213_useFor_MatchFiles.sav'.
match files
 / file = 'da07213_aggregate2.sav'
 / file = 'da07213_useFor_MatchFiles.sav'
 / by V520005.
save outfile = 'da07213_MatchFiles.sav'.
execute.";


            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);

            Assert.IsType<MergeDatasets>(program.Commands[3]);
            var merge = program.Commands[3] as MergeDatasets;

            bool hasAnyMissingTypes = merge.MergeFiles.Any(x => string.IsNullOrWhiteSpace(x.MergeType));
            Assert.False(hasAnyMissingTypes);
        }

        [Fact]
        public void Issue_32()
        {
            string syntax = @"*rename variables in a dataset

get file=""da7568 - 1.sav"".
rename variables
 / (v4 = caseid)
 / (v8 = sex)
 / (v9 = race)
 / (v10 = age)
 / (v11 = marital)
 / (v12 = healthdefect)
 / (v14 = reading)
 / (v15 = writing).
save outfile = ""da7568-1_rename.sav"".
";

            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);

            Assert.Empty(program.Messages);
        }

        [Fact]
        public void Issue_44()
        {
            // SAVE without KEEP.
            string noKeepSyntax = @"get file='da07213-0001.sav'.
save outfile='da07213_useFor_MatchFiles.sav'.
";

            var noKeepConverter = new SpssConverter();
            var noKeepProgram = noKeepConverter.ConvertString(noKeepSyntax);

            Assert.Equal(2, noKeepProgram.CommandCount);


            // SAVE with KEEP.
            string keepSyntax = @"get file='da07213-0001.sav'.
save outfile='da07213_useFor_MatchFiles.sav'
 /keep = V520005 V520006 V520041 V520042 V520043 V520128 V520129 V520142 V520160.
";

            var keepConverter = new SpssConverter();
            var keepProgram = keepConverter.ConvertString(keepSyntax);

            Assert.Equal(3, keepProgram.CommandCount);

            Assert.IsType<KeepVariables>(keepProgram.Commands[1]);
            var keepCommand = keepProgram.Commands[1] as KeepVariables;

            Assert.Equal(9, keepCommand.Variables.Count);

        }

        [Fact]
        public void Issue_45()
        {
            string syntax = @"DO REPEAT existVar=firstVar TO var5
/newVar=new1 TO new5 
/value=1 TO 5.
COMPUTE newVar=existVar*value. 
END REPEAT.
";

            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);

            Assert.Empty(program.Messages);
        }


        [Fact]
        public void Issue_20190404_1()
        {
            string syntax = @"*delete variables from a dataset.
get file=""da7568_SelectCases.sav"".
delete variables AGE MARITAL.
save outfile=""da7568_DeleteVariables.sav"".

*how do we mark up the resulting (output) DDI? At file level, or do we keep the variables listed (no IDs in the new file?) and marked as 
deleted, or both?";

            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);

            Assert.Empty(program.Messages);
        }

        [Fact]
        public void Issue_20190404_2()
        {
            string syntax = @"*create a new dataset that includes aggregations of Age (variable V520142) and INCOME, sorted by Region of Interview.get file='da07213_useForAggregate.sav'.
aggregate
  /outfile='07213_aggregate.sav'
  /break=V520005
  /MED_INC=median(INCOME)
  /MEAN_AGE=mean (V520142).
  execute.

";

            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);

            Assert.Empty(program.Messages);
        }

        [Fact]
        public void Issue_65()
        {
            string syntax = @"* Encoding: UTF-8.
CD ""D: \Users\altergc\Documents\ICPSR\Project development\metadata_capture\scripts\GSS_36797\MVP_scripts\GSS_SPSS"".

GET FILE = '36797-0001-Data-C2M.sav'.

comment * *********RECODE * *********************.
compute age_rec = age.
RECODE age_rec(15 thru 29.999 = 15) (30 thru 49.999 = 30) (50 thru Highest = 50).

VARIABLE LABELS age_rec ""Age recoded by RECODE"".
value labels age_rec 15 ""15-29"" 30 ""30-49"" 50 ""50+"".

RENAME VARIABLES(age_rec = age_3cat).

***********COMPUTE * *************************.
compute age_comp = 10 * trunc(age / 10).
VARIABLE LABELS age_comp ""Age categorized by COMPUTE"".


* *********Expression variants * ***************.
COMPUTE mn_party = MEAN(PARTYID1, PARTYID2, PARTYID3).
COMPUTE agestd = ((age - 45.99) / .07).



* **********tables * ************************.
FREQUENCIES age_3cat age_comp.

EXECUTE.

* *********IF * ****************************.
IF age gt 70  age_comp = 70.

FREQUENCIES age_comp.

EXECUTE.

* *******SELECT  CASES * **********************.
SELECT IF age GE 20.
select if age < 50.

FREQUENCIES age_comp.

EXECUTE.

* ********SELECT VARIABLES * ***************.
DELETE VARIABLES abany to absingle.
delete variables divlaw divlawy.

EXECUTE.

* ******DEFINE MISSING VALUES ******************.
FREQUENCIES PARTYID1 TO PARTYID3.
MISSING VALUES PARTYID1 TO PARTYID3(0, 4 THRU 9).

FREQUENCIES PARTYID1 TO PARTYID3.

************SORT VARIABLES * ****************.
* ***NOTE-- SPSS is not case sensitive * ******.
COMPUTE XX1 = 1.
COMPUTE Xx2 = 2.
COMPUTE xx3 = 3.
COMPUTE xX4 = 4.
COMPUTE XX5 = 5.

SORT VARIABLES by NAME.

*****drop XX1 XX5 Xx2 * ********************.
DELETE VARIABLES Xx2 to XX5.

* ******AGGREGATE * *********************************.
AGGREGATE
       / OUTFILE = *MODE = ADDVARIABLES
       / BREAK = cohort year
        / cy_inc_mean = mean(income)
        / cy_inc_n = nu(income).

  execute.

FREQUENCIES cy_inc_n.
MEANS
   / tables = cy_inc_mean
   / tables = cy_inc_n.

EXECUTE.


* *******FLOW CONTROL-- LOOP BY VARIABLE * ****************.
COMPUTE democrats = 0.
COMPUTE republicans = 0.
DO REPEAT partyidk = PARTYID1 PARTYID2 PARTYID3.
      IF(partyidk EQ 1) democrats = democrats + 1.
     IF(partyidk EQ 2) republicans = republicans + 1.
   END REPEAT.

CROSSTABS
   / tables = democrats BY republicans.

EXECUTE.

* ******FLOW CONTROL-- LOOP BY NUMBER * *****************.
compute idealfam = -1.
missing value idealfam(-1).
LOOP #k= 0 to 9.
  if (#k = chldidel) idealfam= #k.
  END LOOP.

CROSSTABS
  / tables = chldidel by idealfam.

EXECUTE.

* ***********FLOW CONTROL-- LOOP WHILE ****************.
COMPUTE KMORE = 0.
LOOP IF(CHLDMORE EQ 1 AND CHLDNUM GE 0).
COMPUTE KMORE = KMORE + 1.
END LOOP IF(KMORE EQ CHLDNUM).

FREQUENCIES CHLDNUM KMORE.

EXECUTE.

";

            var converter = new SpssConverter();
            var program = converter.ConvertString(syntax);

            Assert.Empty(program.Messages);
        }

    }
}

/*

    *create a new dataset that includes aggregations of Age (variable V520142) and INCOME, sorted by Region of Interview.get file='da07213_useForAggregate.sav'.\naggregate\n /outfile='07213_aggregate.sav'.\n /break=V520005\n /MED_INC=median(INCOME)\n /MEAN_AGE=mean (V520142).\nexecute.\n\n\n\n\n\n\n\n\n\n\n\n
 */
