﻿using C2Metadata.Common.SpssConverter;
using C2Metadata.SpssToSdtl.Tests.Utility;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class IfTests
    {
        [Fact]
        public void If_UseCase1()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("if (V520128 = 1 and V520129 = 1) SEXRACE=1.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<IfRows>(commandList.Commands[0]);
            var ifCommand = commandList.Commands[0] as IfRows;

            Assert.Single(ifCommand.ThenCommands);
            var computeCommand = ifCommand.ThenCommands[0] as Compute;

            TestHelper.AssertSingleVariableWithName(computeCommand.Variable, "SEXRACE");
            Assert.Equal("1", (computeCommand.Expression as NumericConstantExpression).Value);
            //Assert.Equal("(V520128 == 1 AND V520129 == 1)", command.Condition.ToString());
        }

        [Fact]
        public void If_Correct_Command()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("IF age gt 70 age_comp = 70.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<IfRows>(commandList.Commands[0]);
            var ifCommand = commandList.Commands[0] as IfRows;
            Assert.Single(ifCommand.ThenCommands);
            Assert.IsType<Compute>(ifCommand.ThenCommands[0]);
            var computeCommand = ifCommand.ThenCommands[0] as Compute;

            Assert.Equal("ifRows", ifCommand.Command);
            TestHelper.AssertSingleVariableWithName(computeCommand.Variable, "age_comp");
            Assert.NotNull(ifCommand.Condition);
            Assert.IsType<FunctionCallExpression>(ifCommand.Condition);

            var gtCondition = ifCommand.Condition as FunctionCallExpression;

            Assert.Equal(2, gtCondition.Arguments.Count);
            Assert.IsType<VariableSymbolExpression>(gtCondition.Arguments[0].ArgumentValue);
            Assert.IsType<NumericConstantExpression>(gtCondition.Arguments[1].ArgumentValue);
        }

        [Fact]
        public void If_UseCase2()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString(@"*executing a compute that uses the if_then condition to create a new variable that combines categorical values 
* from two source variables.
* declaring missing values on the new variable.
* assigning value labels on the new variable.

get file = 'da07213_useForIf_Then.sav'.
compute SEXRACE = 0.
if (V520128 = 1 and V520129 = 1) SEXRACE = 1.
if (V520128 = 1 and V520129 = 2) SEXRACE = 2.
if (V520128 = 2 and V520129 = 1) SEXRACE = 3.
if (V520128 = 2 and V520129 = 2) SEXRACE = 4.
missing values SEXRACE(0).
value labels SEXRACE 1 'White Male' 2 'Black Male' 3 'White Female' 4 'Black Female'.
save outfile = 'da07213_IfThen_compute.sav'.
execute.
");
            int count = commandList.Commands.Count;
            Assert.Equal(12, count);

            //Assert.IsType<VariableLabelCommand>(commandList.Commands[0]);
            //var command = commandList.Commands[0] as VariableLabelCommand;

        }
    }
}
