﻿using C2Metadata.Common.SpssConverter;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class AddFilesTests
    {
        [Fact]
        public void AddFilesLabel_UseCase1()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString(@"add files
  /file = ""da7568-1_rename.sav""
  /file = ""da7568-2_rename.sav"".
");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<AppendDatasets>(commandList.Commands[0]);
            var command = commandList.Commands[0] as AppendDatasets;

            Assert.Equal(2, command.AppendFiles.Count);
            var appendInfo1 = command.AppendFiles[0];
            var appendInfo2 = command.AppendFiles[1];

            Assert.Equal("da7568-1_rename.sav", appendInfo1.FileName);
            Assert.Equal("da7568-2_rename.sav", appendInfo2.FileName);
        }
    }
}
