﻿using C2Metadata.Common.SpssConverter;
using C2Metadata.SpssToSdtl.Tests.Utility;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class FormatsTests
    {
        [Fact]
        public void PrintFormat_UseCase1()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("print formats Logpop (f3.2).");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<SetDisplayFormat>(commandList.Commands[0]);
            var command = commandList.Commands[0] as SetDisplayFormat;

            Assert.Single(command.Variables);
            TestHelper.AssertSingleVariableWithName(command.Variables, "Logpop");
            Assert.Equal("f3.2", command.DisplayFormatName);
        }
    }
}
