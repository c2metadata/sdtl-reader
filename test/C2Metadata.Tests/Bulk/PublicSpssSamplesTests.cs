using C2Metadata.Common.SpssConverter;
using C2Metadata.Cli;
using C2Metadata.SpssToSdtl.Tests.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;
using Xunit.Abstractions;
using Xunit.Extensions;

namespace C2Metadata.SpssToSdtl.Tests.Bulk
{
    public class PublicSpssSamplesTests
    {
        private ITestOutputHelper output;

        public static IEnumerable<object[]> Paths
        {
            get
            {
                var paths = FileSystemHelpers.GatherSpssCommandFiles(@"c:/svn/ccmetadata/spss-samples-public");

                foreach (string path in paths)
                {
                    yield return new object[] { path };
                }
            }
        }

        public PublicSpssSamplesTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Theory]
        [MemberData(nameof(Paths))]
        public void RunConverterOnSpssSample(string fileName)
        {
            this.output.WriteLine("==============================\n\n\n");
            this.output.WriteLine("Testing " + fileName);

            var converter = new SpssConverter();
            var program = converter.ConvertFile(fileName, true);
            int lines = program.Commands.Count;

            this.output.WriteLine("Messages:\n================\n" + string.Join("\n\n", program.Messages.Select(x => $"{x.LineNumber}:{x.CharacterPosition} - {x.MessageText}")));
            Assert.Empty(program.Messages);

            this.output.WriteLine($"{lines} lines");
            this.output.WriteLine("==============================\n\n\n");
        }

    }
}