﻿using C2Metadata.Common.SpssConverter;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class SaveTests
    {
        [Fact]
        public void SavePlain()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString(@"SAVE OUTFILE='/tmp/out.sav'");
            Assert.Single(commandList.Commands);
        }

        [Fact]
        public void SaveWithKeep()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString(@"SAVE OUTFILE='/tmp/out.sav'
 / KEEP = var1 var2 var3");
            Assert.Equal(2, commandList.Commands.Count);

        }

        [Fact]
        public void SaveWithKeepAndCompressed()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString(@"SAVE OUTFILE='/tmp/out.sav'
 / KEEP = var1 var2 var3
 / COMPRESSED.");
            Assert.Equal(2, commandList.Commands.Count);

        }
    }
}
