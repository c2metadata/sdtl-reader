﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using sdtl;

namespace C2Metadata.Common.SpssConverter
{
    public class SpssFunctionMapper
    {
        public SpssFunctionMapper()
        {
        }

        public FunctionCallExpression CreateFunctionCallExpression(string functionName, List<ExpressionBase> expressionList)
        {
            var result = new FunctionCallExpression();

            (string name, bool isSdtl) = GetSdtlFunctionNameFromSpss(functionName);
            result.Function = name;
            result.IsSdtlName = isSdtl;

            int i = 1;
            foreach (var expression in expressionList)
            {
                var arg = new FunctionArgument();
                arg.ArgumentName = $"EXP{i}";
                arg.ArgumentValue = expression;
                result.Arguments.Add(arg);

                i++;
            }

            return result;
        }

        private (string, bool) GetSdtlFunctionNameFromSpss(string functionName)
        {
            string lower = functionName.ToLower();

            // Sum.[#] just maps to "sum".
            if (Regex.IsMatch(lower, @"sum\.\d"))
            {
                return ("sum", true);
            }

            // Direct mappings from SPSS to SDTL.
            if (functionNameMap.ContainsKey(lower))
            {
                return (functionNameMap[lower], true);
            }

            return (lower, false);
        }


        Dictionary<string, string> functionNameMap = new Dictionary<string, string>
        {
            { "abs", "absolute_value"},
            { "+", "addition"},
            { "cfvar", "coefficient_of_variation"},
            { "cos", "cosine"},
            { "/", "division"},
            { ".", "dot_operator"},
            { "exp", "exponentiation"},
            { "lg10", "logarithm_base_10"},
            { "max", "max"},
            { "mean", "mean"},
            { "median",  "median "},
            { "min", "min"},
            { "mod", "modulo"},
            { "*", "multiplication"},
            { "ln", "natural_logarithm"},
            { "^", "power"},
            { "rv.uniform","random_variable_uniform"},
            { "rnd", "round"},
            { "sin", "sine"},
            { "sqrt",  "square_root"},
            { "sd", "standard_deviation"},
            { "-", "subtraction"},
            { "sum", "sum"},
            { "tan", "tangent"},
            { "trunc", "truncate"},
            { "$sysmis", "missing_value"},
            { "variance", "variance"},
            { "nmiss", "rowmiss"},
            { "nvalid", "rownonmiss"},
            { "value", "value"},
            { "pgt", "agg_pgt"},
            { "plt", "agg_plt"},
            { "pin", "agg_pin"},
            { "pout", "agg_pout"},
            { "fgt", "agg_fgt"},
            { "flt", "agg_flt"},
            { "fin", "agg_fin"},
            { "fout", "agg_fout"},
            { "cgt", "agg_cgt"},
            { "clt", "agg_clt"},
            { "cin", "agg_cin"},
            { "cout", "agg_cout"},
            { "n", "agg_n"},
            { "nu", "agg_count"},
            { "numiss", "agg_numiss"},
            { "first", "agg_first"},
            { "last", "agg_last"},
            { "range", "lgcl_range" },
            { "any", "lgcl_any" },
            { "missing", "missing_var" },
            { "sysmis", "missing_num" }

        };
    }
}