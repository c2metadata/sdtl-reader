﻿using Antlr4.Runtime;
using Antlr4.Runtime.Atn;
using Antlr4.Runtime.Dfa;
using Antlr4.Runtime.Sharpen;
using Antlr4.Runtime.Tree;
using C2Metadata.Common.Model;
using C2Metadata.Common.Utility;
using C2Metadata.SpssToSdtl.Grammar;
using sdtl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using static C2Metadata.SpssToSdtl.Grammar.SpssParser;

namespace C2Metadata.Common.SpssConverter
{
    public class SpssConverter
    {
        private string fileName;
        private bool isVerbose;

        public SpssConverter()
        {

        }

        public Program ConvertFile(string fileName, bool isVerbose = false)
        {
            this.fileName = fileName;
            this.isVerbose = isVerbose;

            string content = File.ReadAllText(fileName);

            Program program = Convert(content);
            AddGeneralProgramInformation(program, content);

            program.SourceFileName = Path.GetFileName(fileName);
            var fileInfo = new FileInfo(fileName);
            program.SourceFileLastUpdate = fileInfo.LastWriteTimeUtc;
            program.SourceFileSize = fileInfo.Length;

            return program;
        }

        public Program ConvertString(string content, bool isVerbose = false)
        {
            this.isVerbose = isVerbose;
            Program program = Convert(content);
            AddGeneralProgramInformation(program, content);

            program.SourceFileName = string.Empty;
            program.SourceFileLastUpdate = TimeHelper.GetUtcNowWithReasonablePrecision();
            program.SourceFileSize = content.Length;

            return program;
        }

        private void AddGeneralProgramInformation(Program program, string content)
        {
            if (program.Commands != null)
            {
                program.CommandCount = program.Commands.Count;
            }

            program.LineCount = content.Split('\n').Length;
            program.ModelCreatedTime = TimeHelper.GetUtcNowWithReasonablePrecision();
            program.ModelVersion = VersionInformation.Version;
            program.Parser = "spss-to-sdtl";
            program.ParserVersion = VersionInformation.Version;
            program.ScriptMD5 = HashHelper.GetMd5Hash(content);
            program.ScriptSHA1 = HashHelper.GetSha1Hash(content);
        }

        private Program Convert(string content)
        {
            // Prefix with a newline.
            content = PreprocessSpssContent(content);

            Program program = new Program();
            program.SourceLanguage = "spss";

            using (var reader = new MemoryStream(Encoding.UTF8.GetBytes(content ?? "")))
            {
                try
                {
                    // Tokenize and parse.
                    var inputStream = new AntlrInputStream(reader);
                    var lexer = new SpssLexer(inputStream);
                    var tokens = new CommonTokenStream(lexer);
                    var parser = new SpssParser(tokens);

                    var errorListener = new MessageTrackingErrorListener();
                    parser.AddErrorListener(errorListener);

                    ProgramContext tree = parser.program();

                    // Add messages from the parser.
                    foreach (var message in errorListener.Messages)
                    {
                        program.Messages.Add(message);
                    }

                    // Visit everything in tree to build the commands.
                    var commandVisitor = new SpssCommandVisitor();
                    commandVisitor.SourceFileName = fileName;
                    var commandList = commandVisitor.VisitProgram(tree)
                        as CommandList;

                    // Add messages from the tree visitor.
                    foreach (var message in commandVisitor.Messages)
                    {
                        program.Messages.Add(message);
                    }

                    program.Commands.AddRange(commandList.Commands);

                    // Very verbose output, if requested.
                    if (isVerbose)
                    {
                        Console.WriteLine("Tokens");
                        Console.WriteLine("----------");
                        foreach (var token in tokens.GetTokens())
                        {
                            Console.WriteLine(token.ToString());
                        }

                        Console.WriteLine("Tree");
                        Console.WriteLine("----------");
                        foreach (var kid in tree.children)
                        {
                            Console.WriteLine(kid.ToStringTree(parser));
                        }
                    }

                    return program;
                }
                catch (Exception ex)
                {
                    var msg = new Message
                    {
                        Severity = "Error",
                    };
                    msg.MessageText.Add("Unhandled error. " + ex.Message);
                    program.Messages.Add(msg);

                    return program;
                }
            }
        }

        private string PreprocessSpssContent(string content)
        {
            content = '\n' + content + '\n';
            content = content.Replace("\r", string.Empty);

            // '-' and '+' at the beginning of lines can be disregarded.
            content = Regex.Replace(content, "^[-+]", string.Empty, RegexOptions.Multiline);

            return content;
        }
    }

}
