﻿using sdtl;
using System;
using System.Collections.Generic;
using System.Text;

namespace C2Metadata.Common.Model
{
    public class CommandList : CommandBase
    {
        public List<CommandBase> Commands { get; } = new List<CommandBase>();
    }
}
