﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sdtl
{
    public partial class CommandBase
    {
        public string GetCommandName()
        {
            if (string.IsNullOrWhiteSpace(TypeDescriminator))
            {
                return string.Empty;
            }

            return char.ToLowerInvariant(TypeDescriminator[0]) + TypeDescriminator.Substring(1);
        }
    }

    public partial class TransformBase
    {

    }

    public partial class Program
    {
        public static int programCount = 1;

        public Program()
        {
            ID = "program-" + programCount++;
        }
    }

    public partial class Aggregate
    {
        public Aggregate()
        {
            Command = GetCommandName();
        }
    }

    public partial class Analysis
    {
        public Analysis()
        {
            Command = GetCommandName();
        }
    }

    public partial class AppendDatasets
    {
        public AppendDatasets()
        {
            Command = GetCommandName();
        }
    }

    public partial class Collapse
    {
        public Collapse()
        {
            Command = GetCommandName();
        }
    }

    public partial class LoopOverList
    {
        public LoopOverList()
        {
            Command = GetCommandName();
        }
    }

    public partial class Unsupported
    {
        public Unsupported()
        {
            Command = GetCommandName();
        }
    }

    public partial class SetDatasetProperty
    {
        public SetDatasetProperty()
        {
            Command = GetCommandName();
        }
    }

    public partial class Invalid
    {
        public Invalid()
        {
            Command = GetCommandName();
        }
    }

    public partial class DoIf
    {
        public DoIf()
        {
            Command = GetCommandName();
        }
    }

    public partial class Execute
    {
        public Execute()
        {
            Command = GetCommandName();
        }
    }

    public partial class Rename
    {
        public Rename()
        {
            Command = GetCommandName();
        }
    }

    public partial class ReshapeLong
    {
        public ReshapeLong()
        {
            Command = GetCommandName();
        }
    }

    public partial class Compute
    {
        public Compute()
        {
            Command = GetCommandName();
        }
    }

    public partial class Load
    {
        public Load()
        {
            Command = GetCommandName();
        }
    }

    public partial class Save
    {
        public Save()
        {
            Command = GetCommandName();
        }
    }

    public partial class ReshapeWide
    {
        public ReshapeWide()
        {
            Command = GetCommandName();
        }
    }

    public partial class Recode
    {
        public Recode()
        {
            Command = GetCommandName();
        }
    }

    public partial class RecodeRule
    {
        public override string ToString()
        {
            string from = string.Empty;
            if (FromValue.Count > 0)
            {
                from = string.Join(", ", FromValue);
            }

            string to = string.Empty;
            if (!string.IsNullOrWhiteSpace(To))
            {
                to = To;
            }

            if (string.IsNullOrWhiteSpace(Label))
            {
                return $"{from} -> {to}"; 
            }
            else
            {
                return $"{from} -> {to} ({Label})"; 
            }
        }
    }

    public partial class SetDisplayFormat
    {
        public SetDisplayFormat()
        {
            Command = GetCommandName();
        }
    }

    public partial class MergeDatasets
    {
        public MergeDatasets()
        {
            Command = GetCommandName();
        }
    }

    public partial class SetMissingValues
    {
        public SetMissingValues()
        {
            Command = GetCommandName();
        }
    }

    public partial class SetVariableLabel
    {
        public SetVariableLabel()
        {
            Command = GetCommandName();
        }
    }

    public partial class SetValueLabels
    {
        public SetValueLabels()
        {
            Command = GetCommandName();
        }
    }

    public partial class Comment
    {
        public Comment()
        {
            Command = GetCommandName();
        }
    }

    public partial class DropVariables
    {
        public DropVariables()
        {
            Command = GetCommandName();
        }
    }

    public partial class KeepVariables
    {
        public KeepVariables()
        {
            Command = GetCommandName();
        }
    }

    public partial class IfRows
    {
        public IfRows()
        {
            Command = GetCommandName();
        }
    }

    public partial class KeepCases
    {
        public KeepCases()
        {
            Command = GetCommandName();
        }
    }

    public partial class NewDataframe
    {
        public NewDataframe()
        {
            Command = GetCommandName();
        }
    }

    public partial class NoTransformOp
    {
        public NoTransformOp()
        {
            Command = GetCommandName();
        }
    }



}
