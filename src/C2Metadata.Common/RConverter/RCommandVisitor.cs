﻿using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using C2Metadata.Common.Model;
using C2Metadata.Common.Utility;
using C2Metadata.RToSdtl.Grammar;
using sdtl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C2Metadata.Common.RConverter
{
    public class RCommandVisitor : RBaseVisitor<CommandBase>
    {
        public string SourceFileName { get; internal set; }

        public List<Message> Messages { get; } = new List<Message>();

        public Dictionary<string, int> FunctionCounts { get; } = new Dictionary<string, int>();

        public override CommandBase VisitProg([NotNull] RParser.ProgContext context)
        {
            return base.VisitProg(context);
        }

        public override CommandBase VisitChildren(IRuleNode node)
        {
            var commandList = new CommandList();

            if (node == null)
            {
                return commandList;
            }

            int n = node.ChildCount;
            for (int i = 0; i < n; i++)
            {
                if (!ShouldVisitNextChild(node, commandList))
                {
                    break;
                }

                var c = node.GetChild(i);
                var childResult = c.Accept(this);
                AggregateResult(commandList, childResult);
            }

            return commandList;
        }

        protected override CommandBase AggregateResult(CommandBase aggregate, CommandBase nextResult)
        {
            if (nextResult == null)
            {
                return aggregate;
            }

            var mainList = aggregate as CommandList;
            if (mainList != null)
            {
                var newList = nextResult as CommandList;
                if (newList != null)
                {
                    foreach (var child in newList.Commands)
                    {
                        if (child != null)
                        {
                            mainList.Commands.Add(child);
                        }
                    }
                }
                else
                {
                    mainList.Commands.Add(nextResult);
                }
            }

            return mainList;
        }

        public override CommandBase VisitCallFunctionExpression([NotNull] RParser.CallFunctionExpressionContext context)
        {
            return ProcessCallFunctionExpression(context, string.Empty);
        }

        public CommandBase ProcessCallFunctionExpression(RParser.CallFunctionExpressionContext context, string dataFrameName)
        {
            string[] analysisCommands = new string[]
            {
                "dim",
                "str",
                "head",
                "describeBy",
                "names",
                "summarize",
                "summarise",
                "ggplot"
            };

            string[] nonTransformCommands = new string[]
            {
                "setwd",
                "library",
                "knitr::opts_chunk$set",
                "set",
            };

            string rFunctionName = context.functionName.Text;

            // Count the function call.
            if (FunctionCounts.ContainsKey(rFunctionName))
            {
                FunctionCounts[rFunctionName]++;
            }
            else
            {
                FunctionCounts.Add(rFunctionName, 1);
            }

            if (analysisCommands.Contains(rFunctionName))
            {
                return HandleUnsupportedCommand(context, UnsupportedCommandType.Analysis);
            }
            else if (nonTransformCommands.Contains(rFunctionName))
            {
                return HandleUnsupportedCommand(context, UnsupportedCommandType.NonTransform);
            }
            else if (rFunctionName == "mutate")
            {
                return HandleMutate(context, dataFrameName);
            }
            else if (rFunctionName == "read_spss" ||
                rFunctionName == "read.csv")
            {
                return HandleLoad(context);
            }
            else if (rFunctionName == "rename")
            {
                return HandleRename(context, dataFrameName);
            }
            else if (rFunctionName == "labelled")
            {
                return HandleLabelled(context);
            }

            return HandleUnsupportedCommand(context, UnsupportedCommandType.Unsupported);
        }

        private CommandBase HandleMutate(RParser.CallFunctionExpressionContext context, string activeDataFrameName)
        {
            var argumentList = context.argumentList;
            string datasetName = activeDataFrameName;
            if (string.IsNullOrWhiteSpace(datasetName))
            {
                // If no data frame name was passed in, the first argument should be an ExpressionArgument with an IdExpression.
                if (!(argumentList._argument[0] is RParser.ExpressionArgumentContext datasetArg))
                {
                    return null;
                }

                datasetName = datasetArg.GetText();
            }

            // The remaining arguments create new variables.
            var assignmentArguments = argumentList._argument
                .OfType<RParser.AssignmentArgumentContext>()
                .ToList();
            if (assignmentArguments == null)
            {
                return null;
            }

            var commandList = new CommandList();
            foreach (RParser.AssignmentArgumentContext assignment in assignmentArguments)
            {
                string variableName = assignment.variableName.Text;

                var compute = new Compute();

                var dataframe = new DataframeDescription()
                {
                    DataframeName = datasetName
                };
                compute.ConsumesDataframe.Add(dataframe);
                compute.Variable = new VariableSymbolExpression { VariableName = variableName };
                compute.Expression = ParseExpression(assignment.assignmentExpr);
                RecordSourceInformation(compute, context);

                commandList.Commands.Add(compute);
            }

            return commandList;
        }

        private TransformBase HandleLoad(RParser.CallFunctionExpressionContext context)
        {
            var argumentList = context.argumentList;
            if (argumentList._argument.Count < 1)
            {
                return null;
            }

            // The first argument should be an ExpressionArgument with a text expression
            if (!(argumentList._argument[0] is RParser.ExpressionArgumentContext fileNameArg))
            {
                return null;
            }

            string fileName = fileNameArg.GetText().StripQuotes();

            var command = new Load();
            command.FileName = fileName;

            return command;
        }

        private TransformBase HandleRename(RParser.CallFunctionExpressionContext context, string activeDataFrameName)
        {
            var argumentList = context.argumentList;
            string datasetName = activeDataFrameName;
            if (string.IsNullOrWhiteSpace(datasetName))
            {
                // If no data frame name was passed in, the first argument should be an ExpressionArgument with an IdExpression.
                if (!(argumentList._argument[0] is RParser.ExpressionArgumentContext datasetArg))
                {
                    return null;
                }

                datasetName = datasetArg.GetText();
            }

            // The remaining arguments create new variables.
            var assignmentArguments = argumentList._argument
                .OfType<RParser.AssignmentArgumentContext>()
                .ToList();
            if (assignmentArguments == null)
            {
                return null;
            }

            var command = new Rename();
            var dataframe = new DataframeDescription { DataframeName = datasetName };
            command.ConsumesDataframe.Add(dataframe);

            foreach (RParser.AssignmentArgumentContext assignment in assignmentArguments)
            {
                string newName = assignment.variableName.Text;
                string oldName = assignment.assignmentExpr.GetText();

                var pair = new RenamePair();
                pair.OldVariable = new VariableSymbolExpression() { VariableName = oldName };
                pair.NewVariable = new VariableSymbolExpression() { VariableName = newName };

                command.Renames.Add(pair);
            }

            RecordSourceInformation(command, context);
            return command;
        }

        private CommandBase HandleLabelled(RParser.CallFunctionExpressionContext context)
        {
            var argumentList = context.argumentList;
            if (argumentList._argument.Count < 2)
            {
                return null;
            }

            // The first argument should be an ExpressionArgument  with an IdExpression.
            if (!(argumentList._argument[0] is RParser.ExpressionArgumentContext targetArg))
            {
                return null;
            }

            string variableName = targetArg.GetText();


            var commandList = new CommandList();

            // The second argument is, a c() of value labels
            if (argumentList._argument[1] is RParser.ExpressionArgumentContext funcExpr &&
                funcExpr.expr() is RParser.CallFunctionExpressionContext funcCallExpr)
            {
                var setValueLabelsCommand = new SetValueLabels();
                setValueLabelsCommand.Variables.Add(new VariableSymbolExpression { VariableName = variableName });

                foreach (RParser.AssignmentArgumentContext valueLabelAssignment in 
                    funcCallExpr.argumentList._argument.OfType<RParser.AssignmentArgumentContext>())
                {
                    string label = valueLabelAssignment.variableName.Text;
                    string value = valueLabelAssignment.assignmentExpr.GetText();

                    setValueLabelsCommand.Labels.Add(new ValueLabel
                    {
                        Value = value,
                        Label = label
                    });
                }

                RecordSourceInformation(setValueLabelsCommand, context);
                commandList.Commands.Add(setValueLabelsCommand);
            }


            // The third argument is a variable label.
            if (argumentList._argument[2] is RParser.ExpressionArgumentContext variableLabelArgument)
            {
                string varLabel = variableLabelArgument.GetText();

                var setVariableLabelCommand = new SetVariableLabel();
                setVariableLabelCommand.Variable = new VariableSymbolExpression { VariableName = variableName };
                setVariableLabelCommand.Label = varLabel;
                RecordSourceInformation(setVariableLabelCommand, context);

                commandList.Commands.Add(setVariableLabelCommand);
            }
            else if (argumentList._argument[2] is RParser.AssignmentArgumentContext varLablAssignmentArg)
            {
                string varLabel = varLablAssignmentArg.assignmentExpr.GetText().StripQuotes();

                var setVariableLabelCommand = new SetVariableLabel();
                setVariableLabelCommand.Variable = new VariableSymbolExpression { VariableName = variableName };
                setVariableLabelCommand.Label = varLabel;
                RecordSourceInformation(setVariableLabelCommand, context);

                commandList.Commands.Add(setVariableLabelCommand);
            }

            return commandList;
        }

        private void ParseUserOpExpression(RParser.UserOpExpressionContext context, CommandList commandList, string sourceDataFrameName, int level)
        {
            if (context.children.Count != 3)
            {
                return;
            }

            if (context.children[1].GetText() != "%>%")
            {
                return;
            }

            string dataFrameName = sourceDataFrameName;

            // Handle left
            if (context.userOpLeft is RParser.IdExpressionContext leftId)
            {
                dataFrameName = leftId.GetText();
            }
            else if (context.userOpLeft is RParser.CallFunctionExpressionContext leftCallFunc)
            {
                var transform = ProcessCallFunctionExpression(leftCallFunc, dataFrameName);
                AggregateResult(commandList, transform);
            }

            // Handle right
            if (context.userOpRight is RParser.CallFunctionExpressionContext rightCallFunc)
            {
                var transform = ProcessCallFunctionExpression(rightCallFunc, dataFrameName);
                AggregateResult(commandList, transform);
            }
            else if (context.userOpRight is RParser.UserOpExpressionContext rightUserOp)
            {
                ParseUserOpExpression(rightUserOp, commandList, dataFrameName, level + 1);
            }
        }

        private ExpressionBase ParseExpression(RParser.ExprContext expression)
        {
            if (expression is RParser.AddExpressionContext addition)
            {
                var result = new FunctionCallExpression();
                result.Function = "addition";
                result.IsSdtlName = true;
                HandleArgumentsForFunctionCallTransformation(addition.expr(), result, "addition");
                return result;
            }
            else if (expression is RParser.SubtractExpressionContext subtract)
            {
                var result = new FunctionCallExpression();
                result.Function = "subtraction";
                result.IsSdtlName = true;
                HandleArgumentsForFunctionCallTransformation(subtract.expr(), result, "subtraction");
                return result;
            }
            else if (expression is RParser.DivideExpressionContext divide)
            {
                var result = new FunctionCallExpression();
                result.Function = "division";
                result.IsSdtlName = true;
                HandleArgumentsForFunctionCallTransformation(divide.expr(), result, "division");
                return result;
            }
            else if (expression is RParser.MultiplyExpressionContext multiply)
            {
                var result = new FunctionCallExpression();
                result.Function = "multiplication";
                result.IsSdtlName = true;
                HandleArgumentsForFunctionCallTransformation(multiply.expr(), result, "multiplication");
                return result;
            }
            else if (expression is RParser.ExponentExpressionContext exponent)
            {
                var result = new FunctionCallExpression();
                result.Function = "exponentiation";
                result.IsSdtlName = true;
                HandleArgumentsForFunctionCallTransformation(exponent.expr(), result, "exponentiation");
                return result;
            }
            else if (expression is RParser.IdExpressionContext id)
            {
                return new VariableSymbolExpression { VariableName = id.GetText() };
            }
            else if (expression is RParser.IntExpressionContext intExpr)
            {
                return new NumericConstantExpression { NumericType = "Integer", Value = intExpr.GetText() };
            }
            else if (expression is RParser.CallFunctionExpressionContext funcExpr)
            {
                string funcName = funcExpr.functionName.Text;

                var result = new FunctionCallExpression();
                result.Function = funcName; // TODO look up and convert from function library
                result.IsSdtlName = false;
                //HandleArgumentsForFunctionCallTransformation(funcExpr.expr(), result, funcName);

                int i = 1;
                foreach (RParser.SubContext arg in funcExpr.argumentList._argument)
                {
                    foreach (IParseTree argChild in arg.children)
                    {
                        if (argChild is RParser.ExprContext argContext)
                        {
                            var argExpr = ParseExpression(argContext);

                            var funcArg = new FunctionArgument();
                            funcArg.ArgumentName = $"EXP{i}";
                            funcArg.ArgumentValue = argExpr;
                            result.Arguments.Add(funcArg);

                            i++;
                        }

                    }

                }

                return result;
            }
            else
            {
                Console.WriteLine("Unhandled expression type: " + expression.GetType().ToString());
            }



            return null;
        }

        private void HandleArgumentsForFunctionCallTransformation(RParser.ExprContext[] expressionTree, FunctionCallExpression expr, string expressionType)
        {
            if (expressionTree.Length > 0)
            {
                var arg = new FunctionArgument();
                arg.ArgumentName = "EXP1";
                arg.ArgumentValue = ParseExpression(expressionTree[0]);
                expr.Arguments.Add(arg);
            }
            if (expressionTree.Length > 1)
            {
                var arg = new FunctionArgument();
                arg.ArgumentName = "EXP2";
                arg.ArgumentValue = ParseExpression(expressionTree[1]);
                expr.Arguments.Add(arg);
            }

            if (expressionTree.Length < 2)
            {
                var msg = new Message
                {
                    Severity = "Warning",
                };
                msg.MessageText.Add($"{expressionType} expression: cannot detect expressions");
                Messages.Add(msg);
            }
        }

        public override CommandBase VisitDefineFunctionExpression([NotNull] RParser.DefineFunctionExpressionContext context)
        {
            return HandleUnsupportedCommand(context, UnsupportedCommandType.Unsupported);
        }

        public override CommandBase VisitCompoundExpression([NotNull] RParser.CompoundExpressionContext context)
        {
            return HandleUnsupportedCommand(context, UnsupportedCommandType.Unsupported);
        }

        public override CommandBase VisitAssignmentExpression([NotNull] RParser.AssignmentExpressionContext context)
        {
            if (context.children.Count == 3)
            {
                if (context.children[2] is RParser.CallFunctionExpressionContext functionCallContext)
                {
                    var transform = VisitCallFunctionExpression(functionCallContext);
                    return transform;
                }
                else if (context.children[2] is RParser.UserOpExpressionContext userOpContext)
                {
                    var commandList = new CommandList();
                    ParseUserOpExpression(userOpContext, commandList, string.Empty, 0);

                    if (commandList.Commands.Count == 0)
                    {
                        return HandleUnsupportedCommand(userOpContext, UnsupportedCommandType.Unsupported);
                    }

                    return commandList;
                }
                else if (context.children[2] is RParser.ExprContext exprContext)
                {
                    var expression = ParseExpression(exprContext);
                    var compute = new Compute();
                    // TODO Mark the dataset to which this computation applies.
                    compute.Variable = new VariableSymbolExpression { VariableName = context.children[0].GetText() };
                    compute.Expression = expression;
                    RecordSourceInformation(compute, context);
                    return compute;
                }
            }

            return HandleUnsupportedCommand(context, UnsupportedCommandType.Unsupported);
        }

        public override CommandBase VisitIfExpression([NotNull] RParser.IfExpressionContext context)
        {
            return HandleUnsupportedCommand(context, UnsupportedCommandType.Unsupported);
        }

        public override CommandBase VisitIfElseExpression([NotNull] RParser.IfElseExpressionContext context)
        {
            return HandleUnsupportedCommand(context, UnsupportedCommandType.Unsupported);
        }

        public override CommandBase VisitForExpression([NotNull] RParser.ForExpressionContext context)
        {
            return HandleUnsupportedCommand(context, UnsupportedCommandType.Unsupported);
        }

        public override CommandBase VisitWhileExpression([NotNull] RParser.WhileExpressionContext context)
        {
            return HandleUnsupportedCommand(context, UnsupportedCommandType.Unsupported);
        }

        public override CommandBase VisitRepeatExpression([NotNull] RParser.RepeatExpressionContext context)
        {
            return HandleUnsupportedCommand(context, UnsupportedCommandType.Unsupported);
        }

        public override CommandBase VisitGetHelpExpression([NotNull] RParser.GetHelpExpressionContext context)
        {
            return HandleUnsupportedCommand(context, UnsupportedCommandType.Unsupported);
        }

        public override CommandBase VisitParenthesizedExpression([NotNull] RParser.ParenthesizedExpressionContext context)
        {
            return HandleUnsupportedCommand(context, UnsupportedCommandType.Unsupported);
        }

        public override CommandBase VisitTerminal(ITerminalNode node)
        {
            if (node.Payload is CommonToken commonToken)
            {
                if (commonToken.Type == RParser.COMMENT)
                {
                    return HandleComment(node);
                }
            }
            return base.VisitTerminal(node);
        }

        private CommandBase HandleComment(ITerminalNode node)
        {
            var result = new Comment();
            result.Command = "comment";

            string text = node.GetText().Trim();
            if (text.StartsWith("#"))
            {
                text = text.Substring(1).Trim();
            }

            result.CommentText = text;

            //RecordSourceInformation(result, node);
            //RecordSourceInformation(result, node.Parent.RuleContext);
            return result;
        }

        private void RecordSourceInformation(CommandBase command, ParserRuleContext context)
        {
            command.SourceInformation = new List<SourceInformation>();
            var source = new SourceInformation();
            command.SourceInformation.Add(source);

            source.SourceStartIndex = context.Start.StartIndex;
            source.SourceStopIndex = context.Stop.StopIndex;
            source.LineNumberStart = context.Start.Line - 1;
            source.LineNumberEnd = context.Stop.Line - 1;
            source.OriginalSourceText = context.Start.InputStream
                .ToString()
                .Substring(context.Start.StartIndex, context.Stop.StopIndex - context.Start.StartIndex + 1)
                .Trim();
        }

        private void RecordSourceInformation(TransformBase command, ITerminalNode node)
        {
            var token = node.Payload as CommonToken;

            command.SourceInformation = new List<SourceInformation>();
            var source = new SourceInformation();
            command.SourceInformation.Add(source);

            source.SourceStartIndex = token.StartIndex;
            source.SourceStopIndex = token.StopIndex;
            source.LineNumberStart = token.Line - 1;
            //source.LineNumberEnd = context.Stop.Line - 1;
            source.OriginalSourceText = token.InputStream
                .ToString()
                .Substring(token.StartIndex, token.StopIndex - token.StartIndex + 1)
                .Trim();
        }

        private CommandBase HandleUnsupportedCommand(Antlr4.Runtime.ParserRuleContext context, UnsupportedCommandType type)
        {
            if (type == UnsupportedCommandType.Analysis)
            {
                var result = new Analysis();
                RecordSourceInformation(result, context);
                return result;
            }
            if (type == UnsupportedCommandType.NonTransform)
            {
                var result = new NoTransformOp();
                RecordSourceInformation(result, context);
                return result;
            }
            else
            {
                var result = new Unsupported();
                result.MessageText.Add("This command is not yet supported.");
                RecordSourceInformation(result, context);
                return result;
            }
        }

    }

    public enum UnsupportedCommandType
    {
        Analysis,
        NonTransform,
        Unsupported
    }
}
