﻿using System;
using System.Collections.Generic;
using System.Text;

namespace C2Metadata.Common.Utility
{
    public static class Extensions
    {
        public static string StripQuotes(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return str;
            }

            str = str.Substring(1, str.Length - 2);
            return str;
        }
    }
}
