﻿using Antlr4.Runtime;
using Antlr4.Runtime.Atn;
using Antlr4.Runtime.Dfa;
using Antlr4.Runtime.Sharpen;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;

namespace C2Metadata.Common.Utility
{
    public class MessageTrackingErrorListener : BaseErrorListener
    {
        public List<Message> Messages { get; } = new List<Message>();

        public override void SyntaxError(IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg, RecognitionException e)
        {
            var message = new Message
            {
                Severity = "Error",
                LineNumber = line - 1,
                CharacterPosition = charPositionInLine,
            };
            message.MessageText.Add(msg);
            Messages.Add(message);
        }

        public override void ReportAmbiguity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, bool exact, BitSet ambigAlts, ATNConfigSet configs)
        {
            
        }

        public override void ReportContextSensitivity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, int prediction, SimulatorState acceptState)
        {
            
        }

        public override void ReportAttemptingFullContext(Parser recognizer, DFA dfa, int startIndex, int stopIndex, BitSet conflictingAlts, SimulatorState conflictState)
        {
            
        }
    }
}
