﻿using System;
using System.Collections.Generic;
using System.Text;

namespace C2Metadata.Common.Utility
{
    public static class TimeHelper
    {
        public static DateTimeOffset GetUtcNowWithReasonablePrecision()
        {
            var now = DateTimeOffset.UtcNow;
            DateTimeOffset then = new DateTimeOffset(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, 0, TimeSpan.FromTicks(0));
            return then;
        }
    }
}
