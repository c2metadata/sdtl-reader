﻿using System;
using System.Collections.Generic;
using System.Text;

namespace C2Metadata.Common
{
    public static class VersionInformation
    {
        public static string Version { get; }  = "0.9 Development";
    }
}
