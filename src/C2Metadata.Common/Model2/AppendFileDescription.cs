using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Describes files used in an AppendDatasets command.
    /// 
    /// <summary>
    public partial class AppendFileDescription
    {

        /// <summary>
        /// Set the TypeDescriminator
        /// <summary>
        public AppendFileDescription() { this.TypeDescriminator = this.GetType().Name; }

        /// <summary>
        /// Type descriminator for json serialization
        /// <summary>
        [JsonProperty("$type")]
        public string TypeDescriminator { get; set; }

        /// <summary>
        /// Name of the file being appended.  May be the name of an active dataframe.
        /// <summary>
        public string FileName { get; set; }
        /// <summary>
        /// Variables to be renamed
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<RenamePair> RenameVariables { get; set; } = new List<RenamePair>();
        public bool ShouldSerializeRenameVariables() { return RenameVariables.Count > 0; }
        /// <summary>
        /// List of variables to keep
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<VariableReferenceBase> KeepVariables { get; set; } = new List<VariableReferenceBase>();
        public bool ShouldSerializeKeepVariables() { return KeepVariables.Count > 0; }
        /// <summary>
        /// List of variables to drop
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<VariableReferenceBase> DropVariables { get; set; } = new List<VariableReferenceBase>();
        public bool ShouldSerializeDropVariables() { return DropVariables.Count > 0; }
        /// <summary>
        /// Logical condition for keeping rows.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public ExpressionBase KeepCasesCondition { get; set; }
        /// <summary>
        /// Logical condition for dropping rows.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public ExpressionBase DropCasesCondition { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            if (FileName != null)
            {
                xEl.Add(new XElement(ns + "FileName", FileName));
            }
            if (RenameVariables != null && RenameVariables.Count > 0)
            {
                foreach (var item in RenameVariables)
                {
                    xEl.Add(item.ToXml("RenameVariables"));
                }
            }
            if (KeepVariables != null && KeepVariables.Count > 0)
            {
                foreach (var item in KeepVariables)
                {
                    xEl.Add(item.ToXml("KeepVariables"));
                }
            }
            if (DropVariables != null && DropVariables.Count > 0)
            {
                foreach (var item in DropVariables)
                {
                    xEl.Add(item.ToXml("DropVariables"));
                }
            }
            if (KeepCasesCondition != null) { xEl.Add(KeepCasesCondition.ToXml("KeepCasesCondition")); }
            if (DropCasesCondition != null) { xEl.Add(DropCasesCondition.ToXml("DropCasesCondition")); }
            return xEl;
        }
    }
}

