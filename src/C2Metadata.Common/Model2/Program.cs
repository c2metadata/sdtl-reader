using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// The program represents a list of commands from a statistical package.
    /// 
    /// <summary>
    public partial class Program : IIdentifiable
    {
        [JsonIgnore]
        public string ReferenceId { get { return $"{ID}"; } }
        /// <summary>
        /// ID of the object being referenced.
        /// <summary>
        public string ID { get; set; }
        /// <summary>
        /// The name of the file containing the source code.
        /// <summary>
        public string SourceFileName { get; set; }
        /// <summary>
        /// The language of the source code.
        /// <summary>
        public string SourceLanguage { get; set; }
        /// <summary>
        /// The MD5 hash of the contents of the file.
        /// <summary>
        public string ScriptMD5 { get; set; }
        /// <summary>
        /// The SHA-1 hash of the contents of the file.
        /// <summary>
        public string ScriptSHA1 { get; set; }
        /// <summary>
        /// The date and time the file was last updated.
        /// <summary>
        [JsonConverter(typeof(DateTimeConverter))]
        public DateTimeOffset SourceFileLastUpdate { get; set; }
        /// <summary>
        /// The size of the file in bytes.
        /// <summary>
        public long SourceFileSize { get; set; }
        /// <summary>
        /// The number of lines in the source file.
        /// <summary>
        public int LineCount { get; set; }
        /// <summary>
        /// The number of commands detected in the source file.
        /// <summary>
        public int CommandCount { get; set; }
        /// <summary>
        /// Messages related to the parsing of the source file.
        /// <summary>
        public List<Message> Messages { get; set; } = new List<Message>();
        public bool ShouldSerializeMessages() { return Messages.Count > 0; }
        /// <summary>
        /// The name of the parser used to generate the SDTL.
        /// <summary>
        public string Parser { get; set; }
        /// <summary>
        /// The version of the parser used to generate the SDTL.
        /// <summary>
        public string ParserVersion { get; set; }
        /// <summary>
        /// The version of the SDTL model.
        /// <summary>
        public string ModelVersion { get; set; }
        /// <summary>
        /// The date and time the SDTL was generated.
        /// <summary>
        [JsonConverter(typeof(DateTimeConverter))]
        public DateTimeOffset ModelCreatedTime { get; set; }
        /// <summary>
        /// The list of commands that make up the program.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<CommandBase> Commands { get; set; } = new List<CommandBase>();
        public bool ShouldSerializeCommands() { return Commands.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml()
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + "Program");
            xEl.Add(new XElement(ns + "ID", ID));
            if (SourceFileName != null)
            {
                xEl.Add(new XElement(ns + "SourceFileName", SourceFileName));
            }
            if (SourceLanguage != null)
            {
                xEl.Add(new XElement(ns + "SourceLanguage", SourceLanguage));
            }
            if (ScriptMD5 != null)
            {
                xEl.Add(new XElement(ns + "ScriptMD5", ScriptMD5));
            }
            if (ScriptSHA1 != null)
            {
                xEl.Add(new XElement(ns + "ScriptSHA1", ScriptSHA1));
            }
            if (SourceFileLastUpdate != default(DateTimeOffset))
            {
                xEl.Add(new XElement(ns + "SourceFileLastUpdate", SourceFileLastUpdate.ToString("yyyy-MM-dd\\THH:mm:ss.FFFFFFFK")));
            }
            xEl.Add(new XElement(ns + "SourceFileSize", SourceFileSize));
            xEl.Add(new XElement(ns + "LineCount", LineCount));
            xEl.Add(new XElement(ns + "CommandCount", CommandCount));
            if (Messages != null && Messages.Count > 0)
            {
                foreach (var item in Messages)
                {
                    xEl.Add(item.ToXml("Messages"));
                }
            }
            if (Parser != null)
            {
                xEl.Add(new XElement(ns + "Parser", Parser));
            }
            if (ParserVersion != null)
            {
                xEl.Add(new XElement(ns + "ParserVersion", ParserVersion));
            }
            if (ModelVersion != null)
            {
                xEl.Add(new XElement(ns + "ModelVersion", ModelVersion));
            }
            if (ModelCreatedTime != default(DateTimeOffset))
            {
                xEl.Add(new XElement(ns + "ModelCreatedTime", ModelCreatedTime.ToString("yyyy-MM-dd\\THH:mm:ss.FFFFFFFK")));
            }
            if (Commands != null && Commands.Count > 0)
            {
                foreach (var item in Commands)
                {
                    xEl.Add(item.ToXml("Commands"));
                }
            }
            return xEl;
        }
    }
}

