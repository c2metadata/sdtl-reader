using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// A numeric constant.
    /// 
    /// <summary>
    public partial class NumericConstantExpression : ExpressionBase
    {
        /// <summary>
        /// A number
        /// <summary>
        public string Value { get; set; }
        /// <summary>
        /// Type of the number
        /// <summary>
        [StringValidation(new string[] {
            "Integer"
,             "Real"
        })]
        public string NumericType { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("ExpressionBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Value != null)
            {
                xEl.Add(new XElement(ns + "Value", Value));
            }
            if (NumericType != null)
            {
                xEl.Add(new XElement(ns + "NumericType", NumericType));
            }
            return xEl;
        }
    }
}

