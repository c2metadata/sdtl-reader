using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// TransformBase defines properties available for transform commands.
    /// 
    /// <summary>
    public abstract partial class TransformBase : CommandBase
    {
        /// <summary>
        /// Signify the dataframe which this transform produces.
        /// <summary>
        public List<DataframeDescription> ProducesDataframe { get; set; } = new List<DataframeDescription>();
        public bool ShouldSerializeProducesDataframe() { return ProducesDataframe.Count > 0; }
        /// <summary>
        /// Signify the dataframe which this transform acts upon.
        /// <summary>
        public List<DataframeDescription> ConsumesDataframe { get; set; } = new List<DataframeDescription>();
        public bool ShouldSerializeConsumesDataframe() { return ConsumesDataframe.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("CommandBase").Descendants())
            {
                xEl.Add(el);
            }
            if (ProducesDataframe != null && ProducesDataframe.Count > 0)
            {
                foreach (var item in ProducesDataframe)
                {
                    xEl.Add(item.ToXml("ProducesDataframe"));
                }
            }
            if (ConsumesDataframe != null && ConsumesDataframe.Count > 0)
            {
                foreach (var item in ConsumesDataframe)
                {
                    xEl.Add(item.ToXml("ConsumesDataframe"));
                }
            }
            return xEl;
        }
    }
}

