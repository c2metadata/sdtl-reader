using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// A reference to a variable.
    /// 
    /// <summary>
    public partial class VariableSymbolExpression : VariableReferenceBase
    {
        /// <summary>
        /// The name of a variable
        /// <summary>
        public string VariableName { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("VariableReferenceBase").Descendants())
            {
                xEl.Add(el);
            }
            if (VariableName != null)
            {
                xEl.Add(new XElement(ns + "VariableName", VariableName));
            }
            return xEl;
        }
    }
}

