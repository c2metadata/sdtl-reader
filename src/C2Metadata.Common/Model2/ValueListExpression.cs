using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Wraps a list of other expressions.
    /// 
    /// <summary>
    public partial class ValueListExpression : ExpressionBase
    {
        /// <summary>
        /// The list of expressions.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<ExpressionBase> Values { get; set; } = new List<ExpressionBase>();
        public bool ShouldSerializeValues() { return Values.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("ExpressionBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Values != null && Values.Count > 0)
            {
                foreach (var item in Values)
                {
                    xEl.Add(item.ToXml("Values"));
                }
            }
            return xEl;
        }
    }
}

