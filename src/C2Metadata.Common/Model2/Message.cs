using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Inserts message text in the SDTL file.
    /// 
    /// <summary>
    public partial class Message : InformBase
    {
        /// <summary>
        /// Information, Warning, Error
        /// <summary>
        public string Severity { get; set; }
        /// <summary>
        /// The line number of the source that the messages is related to, if relevant.
        /// <summary>
        public int LineNumber { get; set; }
        /// <summary>
        /// The character position of the source that the message is related to, if relevant.
        /// <summary>
        public int CharacterPosition { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("InformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Severity != null)
            {
                xEl.Add(new XElement(ns + "Severity", Severity));
            }
            xEl.Add(new XElement(ns + "LineNumber", LineNumber));
            xEl.Add(new XElement(ns + "CharacterPosition", CharacterPosition));
            return xEl;
        }
    }
}

