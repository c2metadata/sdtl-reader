using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Provides a name for the dataframe and an ordered list of variables (optional).   
    /// 
    /// <summary>
    public partial class DataframeDescription
    {
        /// <summary>
        /// Name of the dataframe.
        /// <summary>
        public string DataframeName { get; set; }
        /// <summary>
        /// Variables in the dataframe in order
        /// <summary>
        public List<string> VariableInventory { get; set; } = new List<string>();
        public bool ShouldSerializeVariableInventory() { return VariableInventory.Count > 0; }
        /// <summary>
        /// An ordered list of variables used as hierarchical dimensions in a data cube or multi-index
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<VariableReferenceBase> RowDimensions { get; set; } = new List<VariableReferenceBase>();
        public bool ShouldSerializeRowDimensions() { return RowDimensions.Count > 0; }
        /// <summary>
        /// An ordered list of variables used as colum indexes in a multi-index
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<VariableReferenceBase> ColumnDimensions { get; set; } = new List<VariableReferenceBase>();
        public bool ShouldSerializeColumnDimensions() { return ColumnDimensions.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            if (DataframeName != null)
            {
                xEl.Add(new XElement(ns + "DataframeName", DataframeName));
            }
            if (VariableInventory != null && VariableInventory.Count > 0)
            {
                xEl.Add(
                    from item in VariableInventory
                    select new XElement(ns + "VariableInventory", item.ToString()));
            }
            if (RowDimensions != null && RowDimensions.Count > 0)
            {
                foreach (var item in RowDimensions)
                {
                    xEl.Add(item.ToXml("RowDimensions"));
                }
            }
            if (ColumnDimensions != null && ColumnDimensions.Count > 0)
            {
                foreach (var item in ColumnDimensions)
                {
                    xEl.Add(item.ToXml("ColumnDimensions"));
                }
            }
            return xEl;
        }
    }
}

