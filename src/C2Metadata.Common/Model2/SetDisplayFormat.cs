using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Sets the display or output format for a variable.    
    /// Examples:   
    /// ```
    /// "displayFormatSchema": "SAS"
    /// "displayFormatName": "DOLLAR6.2"
    /// ```
    /// 
    /// ```
    /// "displayFormatSchema": "Stata 15.1"
    /// "displayFormatName": "%tcDDmonCCYY_HH:MM:SS"
    /// ```
    /// 
    /// <summary>
    public partial class SetDisplayFormat : TransformBase
    {
        /// <summary>
        /// The variables that will have their format set
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<VariableReferenceBase> Variables { get; set; } = new List<VariableReferenceBase>();
        public bool ShouldSerializeVariables() { return Variables.Count > 0; }
        /// <summary>
        /// A vendor or standards body with a controlled vocabulary.  The value can be a URL.
        /// <summary>
        public string DisplayFormatSchema { get; set; }
        /// <summary>
        /// The name used in the associated schema.
        /// <summary>
        public string DisplayFormatName { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Variables != null && Variables.Count > 0)
            {
                foreach (var item in Variables)
                {
                    xEl.Add(item.ToXml("Variables"));
                }
            }
            if (DisplayFormatSchema != null)
            {
                xEl.Add(new XElement(ns + "DisplayFormatSchema", DisplayFormatSchema));
            }
            if (DisplayFormatName != null)
            {
                xEl.Add(new XElement(ns + "DisplayFormatName", DisplayFormatName));
            }
            return xEl;
        }
    }
}

