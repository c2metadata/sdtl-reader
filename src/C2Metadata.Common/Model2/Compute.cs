using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Assigns the value of an expression to a variable.  
    /// <summary>
    public partial class Compute : TransformBase
    {
        /// <summary>
        /// The variable that is computed.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase Variable { get; set; }
        /// <summary>
        /// The expression used to compute the value of the variable(s)
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public ExpressionBase Expression { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Variable != null) { xEl.Add(Variable.ToXml("Variable")); }
            if (Expression != null) { xEl.Add(Expression.ToXml("Expression")); }
            return xEl;
        }
    }
}

