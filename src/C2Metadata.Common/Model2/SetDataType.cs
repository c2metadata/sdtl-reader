using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Sets the data type of a variable.   
    /// Examples:   
    /// 
    /// ```
    /// "dataType": "Text",
    /// "subTypeSchema": "Stata 15.1",
    /// "subType": "str1"
    /// ```
    /// 
    /// ```
    /// "dataType": "Numeric",
    /// "subTypeSchema": "https://www.ddialliance.org/Specification/DDI-CV/DataType_1.0.html" ,
    /// "subType": "Int"
    /// ```
    /// 
    /// 
    /// 
    /// <summary>
    public partial class SetDataType : TransformBase
    {
        /// <summary>
        /// The variables that will have their type set
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<VariableReferenceBase> Variables { get; set; } = new List<VariableReferenceBase>();
        public bool ShouldSerializeVariables() { return Variables.Count > 0; }
        /// <summary>
        /// General type of a variable,  e.g.  "Text" or "Numeric".
        /// <summary>
        [StringValidation(new string[] {
            "Text"
,             "Numeric"
,             "Boolean"
,             "Date-Time"
,             "Factor"
        })]
        public string DataType { get; set; }
        /// <summary>
        /// A vendor or standards body with a controlled vocabulary.  The value can be a URL.
        /// <summary>
        public string SubTypeSchema { get; set; }
        /// <summary>
        /// The name used in the associated schema.
        /// <summary>
        public string SubType { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Variables != null && Variables.Count > 0)
            {
                foreach (var item in Variables)
                {
                    xEl.Add(item.ToXml("Variables"));
                }
            }
            if (DataType != null)
            {
                xEl.Add(new XElement(ns + "DataType", DataType));
            }
            if (SubTypeSchema != null)
            {
                xEl.Add(new XElement(ns + "SubTypeSchema", SubTypeSchema));
            }
            if (SubType != null)
            {
                xEl.Add(new XElement(ns + "SubType", SubType));
            }
            return xEl;
        }
    }
}

