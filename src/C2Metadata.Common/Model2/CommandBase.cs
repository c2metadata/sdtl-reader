using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// CommandBase defines general properties available to all commands.
    /// 
    /// <summary>
    public abstract partial class CommandBase
    {

        /// <summary>
        /// Set the TypeDescriminator
        /// <summary>
        public CommandBase() { this.TypeDescriminator = this.GetType().Name; }

        /// <summary>
        /// Type descriminator for json serialization
        /// <summary>
        [JsonProperty("$type")]
        public string TypeDescriminator { get; set; }

        /// <summary>
        /// The type of command
        /// <summary>
        public string Command { get; set; }
        /// <summary>
        /// Information about the source of the command.
        /// <summary>
        public List<SourceInformation> SourceInformation { get; set; } = new List<SourceInformation>();
        public bool ShouldSerializeSourceInformation() { return SourceInformation.Count > 0; }
        /// <summary>
        /// Adds a message that can be displayed with the command.
        /// <summary>
        public List<string> MessageText { get; set; } = new List<string>();
        public bool ShouldSerializeMessageText() { return MessageText.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            if (Command != null)
            {
                xEl.Add(new XElement(ns + "Command", Command));
            }
            if (SourceInformation != null && SourceInformation.Count > 0)
            {
                foreach (var item in SourceInformation)
                {
                    xEl.Add(item.ToXml("SourceInformation"));
                }
            }
            if (MessageText != null && MessageText.Count > 0)
            {
                xEl.Add(
                    from item in MessageText
                    select new XElement(ns + "MessageText", item.ToString()));
            }
            return xEl;
        }
    }
}

