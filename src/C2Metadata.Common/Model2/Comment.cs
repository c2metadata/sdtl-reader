using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Describes a source code comment.  Comments should not include special characters.  
    /// 
    /// <summary>
    public partial class Comment : InformBase
    {
        /// <summary>
        /// The text of the source code comment.
        /// <summary>
        public string CommentText { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("InformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (CommentText != null)
            {
                xEl.Add(new XElement(ns + "CommentText", CommentText));
            }
            return xEl;
        }
    }
}

