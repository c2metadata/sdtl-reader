using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Describes how values will be recoded.
    /// 
    /// <summary>
    public partial class RecodeRule
    {
        /// <summary>
        /// The values to be recoded.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<ExpressionBase> FromValue { get; set; } = new List<ExpressionBase>();
        public bool ShouldSerializeFromValue() { return FromValue.Count > 0; }
        /// <summary>
        /// The new value
        /// <summary>
        public string To { get; set; }
        /// <summary>
        /// A value label for the new recoded value, if appropriate
        /// <summary>
        public string Label { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            if (FromValue != null && FromValue.Count > 0)
            {
                foreach (var item in FromValue)
                {
                    xEl.Add(item.ToXml("FromValue"));
                }
            }
            if (To != null)
            {
                xEl.Add(new XElement(ns + "To", To));
            }
            if (Label != null)
            {
                xEl.Add(new XElement(ns + "Label", Label));
            }
            return xEl;
        }
    }
}

