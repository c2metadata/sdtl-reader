using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Defines a range of string values, such as "A to N".
    /// 
    /// <summary>
    public partial class StringRangeExpression : ExpressionBase
    {
        /// <summary>
        /// Starting value for range
        /// <summary>
        public string RangeStart { get; set; }
        /// <summary>
        /// Ending value for range
        /// <summary>
        public string RangeEnd { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("ExpressionBase").Descendants())
            {
                xEl.Add(el);
            }
            if (RangeStart != null)
            {
                xEl.Add(new XElement(ns + "RangeStart", RangeStart));
            }
            if (RangeEnd != null)
            {
                xEl.Add(new XElement(ns + "RangeEnd", RangeEnd));
            }
            return xEl;
        }
    }
}

