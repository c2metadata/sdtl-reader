using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// A set of commands that are performed on each row in the dataframe when a logical expression is true for that row.  May also include ElseCommands to be performed if the logical expression is false.  Use DoIf for a logical condition that applies to the entire dataframe and commands that are performed once.  
    /// 
    /// <summary>
    public partial class IfRows : TransformBase
    {
        /// <summary>
        /// A logical expression that is evaluated separately for every row in the dataframe.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public ExpressionBase Condition { get; set; }
        /// <summary>
        /// Commands to be performed if the condition is true.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<CommandBase> ThenCommands { get; set; } = new List<CommandBase>();
        public bool ShouldSerializeThenCommands() { return ThenCommands.Count > 0; }
        /// <summary>
        /// Commands to be performed if the condition is false.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<CommandBase> ElseCommands { get; set; } = new List<CommandBase>();
        public bool ShouldSerializeElseCommands() { return ElseCommands.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Condition != null) { xEl.Add(Condition.ToXml("Condition")); }
            if (ThenCommands != null && ThenCommands.Count > 0)
            {
                foreach (var item in ThenCommands)
                {
                    xEl.Add(item.ToXml("ThenCommands"));
                }
            }
            if (ElseCommands != null && ElseCommands.Count > 0)
            {
                foreach (var item in ElseCommands)
                {
                    xEl.Add(item.ToXml("ElseCommands"));
                }
            }
            return xEl;
        }
    }
}

