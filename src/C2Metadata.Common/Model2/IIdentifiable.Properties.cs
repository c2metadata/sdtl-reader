using System;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace sdtl
{
    /// <summary>
    /// IIdentifiable class which all object Inherit from. Used to Serialize to Json
    /// <summary>
    public partial interface IIdentifiable
    {
        string ID { get; set; }
    }
}
