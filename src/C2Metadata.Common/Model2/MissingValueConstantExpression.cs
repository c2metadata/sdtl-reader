using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// A missing value constant.  Some languages allow multiple missing value constants.
    /// 
    /// <summary>
    public partial class MissingValueConstantExpression : ExpressionBase
    {
        /// <summary>
        /// The missing value as it appears in the system (e.g., .a, .b, .c).  If Null, this is the system missing value.
        /// <summary>
        public string Value { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("ExpressionBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Value != null)
            {
                xEl.Add(new XElement(ns + "Value", Value));
            }
            return xEl;
        }
    }
}

