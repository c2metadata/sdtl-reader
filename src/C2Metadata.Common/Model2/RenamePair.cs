using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Variable names before and after a variable is renamed.
    /// 
    /// <summary>
    public partial class RenamePair
    {

        /// <summary>
        /// Set the TypeDescriminator
        /// <summary>
        public RenamePair() { this.TypeDescriminator = this.GetType().Name; }

        /// <summary>
        /// Type descriminator for json serialization
        /// <summary>
        [JsonProperty("$type")]
        public string TypeDescriminator { get; set; }

        /// <summary>
        /// The old name of the variable.
        /// <summary>
        public VariableSymbolExpression OldVariable { get; set; }
        /// <summary>
        /// The new name of the variable.
        /// <summary>
        public VariableSymbolExpression NewVariable { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            if (OldVariable != null) { xEl.Add(OldVariable.ToXml("OldVariable")); }
            if (NewVariable != null) { xEl.Add(NewVariable.ToXml("NewVariable")); }
            return xEl;
        }
    }
}

