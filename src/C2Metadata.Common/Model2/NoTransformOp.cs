using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// NoTransformOp is used for a command in the original script that provides important information but does not have a function in SDTL.  For example, “library()” in R loads a package of R functions.  Since the Parser detects the library, the SDTL will reflect the library that is used, and commands derived from the library will be translated in the SDTL script.  However, it is useful to know which library is active for auditing the R script, even if it does not perform any data transformations.  
    /// 
    /// <summary>
    public partial class NoTransformOp : InformBase
    {

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("InformBase").Descendants())
            {
                xEl.Add(el);
            }
            return xEl;
        }
    }
}

