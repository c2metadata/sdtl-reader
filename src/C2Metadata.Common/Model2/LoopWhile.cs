using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// LoopWhile iterates over a set of commands under the control of one or more logical expressions.  Since the logical conditions typically depend upon values in the data, commands executed in a LoopWhile cannot be anticipated and expanded in SDTL.
    /// <summary>
    public partial class LoopWhile : TransformBase
    {
        /// <summary>
        /// Describes a condition required for the next iteration to begin.  
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public ExpressionBase Condition { get; set; }
        /// <summary>
        /// Describes a condition that ends interation.  
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public ExpressionBase EndCondition { get; set; }
        /// <summary>
        /// Commands within the loop.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<CommandBase> Commands { get; set; } = new List<CommandBase>();
        public bool ShouldSerializeCommands() { return Commands.Count > 0; }
        /// <summary>
        /// When TRUE, the loop has been expanded into separate commands.
        /// <summary>
        public bool Updated { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Condition != null) { xEl.Add(Condition.ToXml("Condition")); }
            if (EndCondition != null) { xEl.Add(EndCondition.ToXml("EndCondition")); }
            if (Commands != null && Commands.Count > 0)
            {
                foreach (var item in Commands)
                {
                    xEl.Add(item.ToXml("Commands"));
                }
            }
            xEl.Add(new XElement(ns + "Updated", Updated));
            return xEl;
        }
    }
}

