using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// An expression evaluated by reference to the Function Library.
    /// <summary>
    public partial class FunctionCallExpression : ExpressionBase
    {
        /// <summary>
        /// The name of the function being called.
        /// <summary>
        public string Function { get; set; }
        /// <summary>
        /// When true, the Function property contains the name of a function from the SDTL function library. When false, the Function property contains the name of a system-specific or user-defined function.
        /// <summary>
        public bool IsSdtlName { get; set; }
        /// <summary>
        /// A list of parameters to the function.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<FunctionArgument> Arguments { get; set; } = new List<FunctionArgument>();
        public bool ShouldSerializeArguments() { return Arguments.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("ExpressionBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Function != null)
            {
                xEl.Add(new XElement(ns + "Function", Function));
            }
            xEl.Add(new XElement(ns + "IsSdtlName", IsSdtlName));
            if (Arguments != null && Arguments.Count > 0)
            {
                foreach (var item in Arguments)
                {
                    xEl.Add(item.ToXml("Arguments"));
                }
            }
            return xEl;
        }
    }
}

