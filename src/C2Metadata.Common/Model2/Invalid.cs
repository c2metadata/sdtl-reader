using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Describes an invalid command. A command is invalid if it uses incorrect syntax, or is otherwise not allowed by the executing system.
    /// 
    /// <summary>
    public partial class Invalid : InformBase
    {
        /// <summary>
        /// A message describing the issue with the invalid command.
        /// <summary>
        public string ProblemDescription { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("InformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (ProblemDescription != null)
            {
                xEl.Add(new XElement(ns + "ProblemDescription", ProblemDescription));
            }
            return xEl;
        }
    }
}

