﻿// Copyright 2017 Colectica.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using Eto;
using Eto.Drawing;
using Eto.Forms;
using sdtl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SdtlReader.Utility
{
    public static class UIBuilder
    {
        public static Label CreateLabel(string text, int size = 12)
        {
            var label = new Label { Text = text };

            if (Platform.Instance.IsWpf)
            {
                label.Font = new Font(new FontFamily("Segoe UI"), size);
            }
            else if (Platform.Instance.IsMac)
            {
                label.Font = new Font(new FontFamily("Helvetica Neue"), size);
            }
            else
            {
                label.Font = new Font(new FontFamily("Sans"), size);
            }

            return label;
        }

        public static Button CreateButton(string text)
        {
            var button = new Button();
            button.Text = text;

            return button;
        }

        public static TableLayout CreateKeyValueTable(params string[] cells)
        {
            if (cells.Length % 2 != 0)
            {
                throw new ArgumentException("cells must have an even number of values");
            }

            var table = new TableLayout();
            table.Spacing = new Size(10, 10);
            table.Padding = new Padding(20, 20);

            for (int i = 0; i < cells.Length; i += 2)
            {
                string key = cells[i];
                string value = cells[i + 1];

                var row = new TableRow();
                row.Cells.Add(key);
                row.Cells.Add(value);
                table.Rows.Add(row);
            }

            return table;
        }

        public static Scrollable MakeScrollable(Control control)
        {
            return new Scrollable
            {
                Content = control
            };
        }

        public static Scrollable MakeScrollable(Control control, Control control2)
        {
            var stack = new StackLayout();
            stack.Items.Add(control);
            stack.Items.Add(control2);

            return new Scrollable
            {
                Content = stack
            };
        }

        public static TableLayout CreateSourceInformationCells(List<SourceInformation> sourceList)
        {
            var source = sourceList.FirstOrDefault();

            if (source != null)
            {
                return CreateKeyValueTable(
                    "Line Number Start", source.LineNumberStart.ToString(),
                    "Line Number End", source.LineNumberEnd.ToString(),
                    "Original Source", source.OriginalSourceText,
                    "Processed Source", source.ProcessedSourceText
                    );
            }

            return null;
        }
    }
}
