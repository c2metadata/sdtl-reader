﻿using sdtl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SdtlReader.Utility
{
    public class VariableGatherer
    {
        public List<VariableOperation> GatherVariables(IEnumerable<CommandBase> commands)
        {
            var result =  new List<VariableOperation>();

            foreach (var transform in commands)
            {
                if (transform is Recode recode)
                {
                    foreach (var var in recode.RecodedVariables)
                    {
                        var op = new VariableOperation();
                        result.Add(op);
                        op.VariableName = var.Source;

                        var lines = recode.Rules.Select(x => $"Recoded: {string.Join(",", x.FromValue)} -> {x.To}");
                        op.Description = string.Join(Environment.NewLine, lines);
                    }
                }
                else if (transform is Rename rename)
                {
                    foreach (var pair in rename.Renames)
                    {
                        var op = new VariableOperation();
                        result.Add(op);
                        op.VariableName = pair.NewVariable.VariableName;
                        op.Description = "Renamed from " + pair.OldVariable.VariableName;

                        var op2 = new VariableOperation();
                        result.Add(op2);
                        op2.VariableName = pair.OldVariable.VariableName;
                        op2.Description = "Renamed to " + pair.NewVariable.VariableName;
                    }
                }
                else if (transform is Compute compute)
                {
                    var op = new VariableOperation();
                    result.Add(op);

                    if (compute.Variable is VariableSymbolExpression varSymbol)
                    {
                        op.VariableName = varSymbol.VariableName;
                    }

                    op.Description = "Computed with " + compute.Expression?.ToString();
                }

            }

            return result;
        }

    }

    public class VariableOperation
    {
        public string VariableName { get; set; }

        public string Description { get; set; }
    }
}
