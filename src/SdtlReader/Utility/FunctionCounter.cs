﻿using sdtl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SdtlReader.Utility
{
    public class FunctionCounter
    {
        private Dictionary<string, int> result;

        public Dictionary<string, int> CountFunctions(IEnumerable<CommandBase> commands)
        {
            this.result = new Dictionary<string, int>();

            foreach (var transform in commands)
            {
                if (transform is Compute compute)
                {
                    ProcessExpression(compute.Expression);
                }
            }

            return result;
        }

        private void ProcessExpression(ExpressionBase expression)
        {
            if (expression is FunctionCallExpression functionCall)
            {
                IncrementFunctionCall(functionCall.Function);

                foreach (var param in functionCall.Arguments)
                {
                    ProcessExpression(param.ArgumentValue);
                }
            }
            else if (expression is GroupedExpression grouped)
            {
                ProcessExpression(grouped.Expression);
            }
        }

        private void IncrementFunctionCall(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return;
            }

            if (!result.ContainsKey(name))
            {
                result.Add(name, 1);
            }
            else
            {
                result[name]++;
            }

        }
    }
}
