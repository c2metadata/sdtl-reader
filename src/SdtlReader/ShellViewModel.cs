﻿// Copyright 2017 Colectica.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using C2Metadata.Common.RConverter;
using C2Metadata.Common.SpssConverter;
using C2Metadata.Common.Utility;
using Eto.Forms;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using sdtl;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SdtlReader
{
    public class ShellViewModel
    {
        public MainForm MainForm { get; set; }

        public ObservableCollection<Program> Programs { get; } = new ObservableCollection<Program>();

        private TransformBase _selectedTransform;
        public TransformBase SelectedTransform
        {
            get => _selectedTransform;
            set
            {
                _selectedTransform = value;
            }
        }

        public void OpenFile()
        {
            var dlg = new OpenFileDialog();
            dlg.Filters.Add(new FileDialogFilter("SPSS Syntax Files", ".sps"));
            dlg.Filters.Add(new FileDialogFilter("R Syntax Files", ".r"));
            dlg.Filters.Add(new FileDialogFilter("R Markdown Files", ".rmd"));
            //dlg.Filters.Add(new FileDialogFilter("JSON Files", ".json"));
            dlg.MultiSelect = true;

            var result = dlg.ShowDialog(null);
            if (result != DialogResult.Ok)
            {
                return;
            }

            Programs.Clear();

            // Read the JSON content into a list of CommandBase objects. Does that work?
            foreach (string fileName in dlg.Filenames)
            {
                if (Path.GetExtension(fileName).ToLower() == ".json")
                {
                    //var programs = GetProgramsFromJson(program, fileName);
                    //foreach (var program in programs)
                    //{
                    //    Programs.Add(program);
                    //}
                }
                else if (Path.GetExtension(fileName).ToLower() == ".sps")
                {
                    var program = GetProgramFromSpss(fileName);
                    program.SourceFileName = fileName;
                    Programs.Add(program);
                }
                else if (Path.GetExtension(fileName).ToLower() == ".r")
                {
                    var program = GetProgramFromR(fileName);
                    program.SourceFileName = fileName;
                    Programs.Add(program);
                }
                else if (Path.GetExtension(fileName).ToLower() == ".rmd")
                {
                    var program = GetProgramFromR(fileName);
                    program.SourceFileName = fileName;
                    Programs.Add(program);
                }
                else
                {
                    // TODO
                }


            }

            MainForm.UpdateTransformsTree();
        }

        public void SaveXml()
        {
            var dlg = new SaveFileDialog();
            dlg.Filters.Add(new FileDialogFilter("XML Files", ".xml"));

            var result = dlg.ShowDialog(null);
            if (result != DialogResult.Ok)
            {
                return;
            }
            string outputFile = dlg.FileName;

            // Add everything to an item container.
            var itemContainer = new ItemContainer();
            foreach (var program in Programs)
            {
                itemContainer.TopLevelReferences.Add(program);
                itemContainer.Items.Add(program);
            }

            var doc = itemContainer.MakeXml();
            doc.Save(outputFile);
        }

        public void SaveJson()
        {
            var dlg = new SaveFileDialog();
            dlg.Filters.Add(new FileDialogFilter("JSON Files", ".json"));

            var result = dlg.ShowDialog(null);
            if (result != DialogResult.Ok)
            {
                return;
            }
            string outputFile = dlg.FileName;

            // Save the JSON file with transform descriptions.
            var itemContainer = new ItemContainer();
            foreach (var program in Programs)
            {
                itemContainer.TopLevelReferences.Add(program);
                itemContainer.Items.Add(program);
            }

            string json = SdtlSerializer.SerializeAsJson(itemContainer);
            File.WriteAllText(outputFile, json);
        }

        private Program GetProgramFromSpss(string fileName)
        {
            var converter = new SpssConverter();
            var program = converter.ConvertFile(fileName, false);
            return program;
        }

        private Program GetProgramFromR(string fileName)
        {
            var converter = new RConverter();
            var program = converter.ConvertFile(fileName, false);
            return program;
        }

        private void AddTransformsFromJson(Program sequence, string fileName)
        {
            string contentStr = File.ReadAllText(fileName);

            var settings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
            };
            var itemContainer = JsonConvert.DeserializeObject<ItemContainer>(contentStr, settings);

            //var token = JToken.Parse(contentStr);
            //if (token is JArray jArray)
            //{
            //    foreach (JObject item in jArray)
            //    {
            //        AddCommand(sequence, item);
            //    }
            //}
            //else if (token is JObject item)
            //{
            //    var childArray = item.GetValue("commands");
            //    foreach (JObject x in childArray)
            //    {
            //        AddCommand(sequence, x);
            //    }
            //}

        }

    }
}
