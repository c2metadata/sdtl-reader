﻿import { Component, Input, ElementRef, forwardRef } from '@angular/core';
import { Services } from './services/services'
import { Spss } from './spssObj/Spss'

@Component({
    selector: 'my-app', 
    template: `
    <ul class="tree" *ngIf="data != null">
        <li class="list">
           <a (click)='onClick(obj)'>Summary</a><input type="checkbox">
        </li>
        <li class="list">
           <a>Content</a><input type="checkbox">
            <ul>
                <li *ngFor="let key of objectKeys(obj[commands])" class="list">
                    <a (click)='onClick(obj[commands][key])'>{{obj[commands][key][command]}}</a>
                </li>
            </ul>
        </li>
    </ul>`
})

export class AppComponent {
    //objectKeys = Object.keys();
    data: string = this.elementRef.nativeElement.getAttribute("data");
    obj: Object = JSON.parse(this.data);
    commands: string = "commands";
    command: string = "command";
    objectKeys = Object.keys;
    constructor(private elementRef: ElementRef, public service: Services) {
    }

    isObj(val: any) {
        return typeof val === 'object';
    }

    onClick(info: any) {
        if (info != null) {
            this.service.sendData(info);
        }
    }
}