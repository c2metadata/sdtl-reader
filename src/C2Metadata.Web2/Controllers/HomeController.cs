﻿using C2Metadata.Common.SpssConverter;
using C2Metadata.Common.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Windows.Forms;

namespace C2Metadata.Web2.Controllers
{
    public class HomeController : Controller
    {
        public static string Json_res { get; set; }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(string submit1, string data, HttpPostedFileBase file, HttpPostedFileBase json_file)
        {
            string info = "";
            string json = "";
            Boolean isJson = false;
            if (file != null)
            {
                BinaryReader b = new BinaryReader(file.InputStream);
                byte[] binData = b.ReadBytes(file.ContentLength);
                info = System.Text.Encoding.UTF8.GetString(binData);
            }
            else if (!string.IsNullOrEmpty(data))
            {
                if (data.Trim().StartsWith("{"))
                {
                    isJson = true;
                }
                info = data;
            }
            else if(json_file != null)
            {
                if (json_file.FileName.Contains(".json"))
                {
                    isJson = true;
                    BinaryReader b = new BinaryReader(json_file.InputStream);
                    byte[] binData = b.ReadBytes(json_file.ContentLength);
                    info = System.Text.Encoding.UTF8.GetString(binData);
                }
            }
            //convert string to json and show in data
            if(!isJson)
            {
                var converter = new SpssConverter();
                var converted = converter.ConvertString(info);
                json = SdtlSerializer.SerializeAsJson(converted);
                Json_res = json;
            }
            if(isJson)
            {
                Json_res = info;
                ViewBag.Data = info;
            }
            else
            {
                ViewBag.Data = json;
            }
            return View();
        }

        public FileResult DownloadJSON()
        {
            string name = "c_sample.json";

            return File(Encoding.UTF8.GetBytes(Json_res),
            System.Net.Mime.MediaTypeNames.Application.Octet,
            name);
        }

        public FileResult DownloadXml()
        {
            string name = "c_sample.xml";

            return File(Encoding.UTF8.GetBytes(Json_res),
            System.Net.Mime.MediaTypeNames.Application.Octet,
            name);
        }

        //[HttpPost]
        //public async Task<ActionResult> spsstostdl()
        //{f
        //string info = string.Empty;
        //using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
        //{
        //    info = await reader.ReadToEndAsync();
        //}
        ////convert string to json and show in data
        //var converter = new BasicConverter();
        //var converted = converter.ConvertString(info);
        //string json = SdtlSerializer.SerializeAsJson(converted);
        //    return Content(json, "application/json");
        //}

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}