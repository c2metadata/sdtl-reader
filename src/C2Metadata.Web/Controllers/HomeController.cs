﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using C2Metadata.Web.Models;
using C2Metadata.Common.SpssConverter;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using C2Metadata.Common.Utility;
using C2Metadata.Common;
using C2Metadata.Web.InputDto;
using C2Metadata.Common.RConverter;

namespace C2Metadata.Web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Spss(string data)
        {
            // Check the inputs.
            string code = data;
            if (string.IsNullOrWhiteSpace(code))
            {
                return StatusCode(400, "No SPSS code was found in the input.");
            }

            // Parse the SPSS.
            var converter = new SpssConverter();
            var program = converter.ConvertString(code);

            // Return some JSON.
            if (program.Messages.Any(x => x.Severity == "Error" || x.Severity == "Warning"))
            {
                return StatusCode(500, program);
            }
            else
            {
                string json = SdtlSerializer.SerializeAsJson(program);
                return Content(json, "application/json");
            }
        }

        [HttpPost]
        public IActionResult R(string data)
        {
            // Check the inputs.
            string code = data;
            if (string.IsNullOrWhiteSpace(code))
            {
                return StatusCode(400, "No SPSS code was found in the input.");
            }

            // Parse the SPSS.
            var converter = new RConverter();
            var program = converter.ConvertString(code);

            // Return some JSON.
            if (program.Messages.Any(x => x.Severity == "Error" || x.Severity == "Warning"))
            {
                return StatusCode(500, program);
            }
            else
            {
                string json = SdtlSerializer.SerializeAsJson(program);
                return Content(json, "application/json");
            }
        }

        [HttpPost("api/spsstosdtl")]
        public async Task <IActionResult> SpssToSdtl()
        {
            // Read the full POST body. We assume it is JSON.
            string inputJson = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                inputJson = await reader.ReadToEndAsync();
            }


            // Check the input.
            if (string.IsNullOrWhiteSpace(inputJson))
            {
                return StatusCode(400, "Input is empty");
            }

            InputRequest inputObj = null;
            try
            {
                inputObj = InputRequest.FromJson(inputJson);
            }
            catch
            {
                return StatusCode(400, "Input could not be parsed as JSON");
            }

            if (inputObj?.Parameters == null)
            {
                return StatusCode(400, "Could not read input.");
            }

            string code = inputObj.Parameters.Spss;
            if (string.IsNullOrWhiteSpace(code))
            {
                return StatusCode(400, "No SPSS code was found in the input.");
            }

            // Parse the SPSS.
            var converter = new SpssConverter();
            var program = converter.ConvertString(inputObj.Parameters.Spss);

            // Return some JSON.
            if (program.Messages.Any(x => x.Severity == "Error" || x.Severity == "Warning"))
            {
                return StatusCode(500, program);
            }
            else
            {
                string json = SdtlSerializer.SerializeAsJson(program);
                return Content(json, "application/json");
            }

        }

        [HttpPost("api/rtosdtl")]
        public async Task <IActionResult> RToSdtl()
        {
            // Read the full POST body. We assume it is JSON.
            string inputJson = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                inputJson = await reader.ReadToEndAsync();
            }


            // Check the input.
            if (string.IsNullOrWhiteSpace(inputJson))
            {
                return StatusCode(400, "Input is empty");
            }

            InputRequest inputObj = null;
            try
            {
                inputObj = InputRequest.FromJson(inputJson);
            }
            catch
            {
                return StatusCode(400, "Input could not be parsed as JSON");
            }

            if (inputObj == null)
            {
                return StatusCode(400, "Could not read input.");
            }

            string code = inputObj.Parameters.RCode;
            if (string.IsNullOrWhiteSpace(code))
            {
                return StatusCode(400, "No R code was found in the input.");
            }

            // Parse the R.
            var converter = new RConverter();
            var program = converter.ConvertString(inputObj.Parameters.RCode);

            // Return some JSON.
            if (program.Messages.Any(x => x.Severity == "Error" || x.Severity == "Warning"))
            {
                return StatusCode(500, program);
            }
            else
            {
                string json = SdtlSerializer.SerializeAsJson(program);
                return Content(json, "application/json");
            }

        }
        [Route("version")]
        public IActionResult Version()
        {
            return Content(VersionInformation.Version);
        }

    }
}
