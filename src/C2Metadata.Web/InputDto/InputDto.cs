﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace C2Metadata.Web.InputDto
{
    public partial class Parameters
    {
        [JsonProperty("data_file_descriptions")]
        public DataFileDescription[] DataFileDescriptions { get; set; }

        [JsonProperty("spss")]
        public string Spss { get; set; }

        [JsonProperty("r")]
        public string RCode { get; set; }
    }

    public partial class DataFileDescription
    {
        [JsonProperty("input_file_name")]
        public string InputFileName { get; set; }

        [JsonProperty("DDI_XML_file")]
        public string DdiXmlFile { get; set; }

        [JsonProperty("variables")]
        public string[] Variables { get; set; }

        [JsonProperty("file_name_DDI")]
        public string FileNameDdi { get; set; }

    }

    public partial class InputRequest
    {
        public Parameters Parameters { get; set; }

        public static InputRequest FromJson(string json) => JsonConvert.DeserializeObject<InputRequest>(json, C2Metadata.Web.InputDto.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this InputRequest self) => JsonConvert.SerializeObject(self, InputDto.Converter.Settings);
        public static string ToJson(this Parameters self) => JsonConvert.SerializeObject(self, InputDto.Converter.Settings);
        public static string ToJson(this DataFileDescription self) => JsonConvert.SerializeObject(self, InputDto.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
